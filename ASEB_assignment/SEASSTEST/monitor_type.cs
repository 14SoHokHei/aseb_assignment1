﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SEASSTEST
{
    class monitor_type
    {
        string monitor;

        public string getMonitorType (int monitor_num)
        {
            switch (monitor_num)
            {
                case 1:
                    monitor = "polar Sport Tester / Vantage XL";
                    break;
                case 2:
                    monitor = "Polar Vantage NV (VNV)";
                    break;
                case 3:
                    monitor = "Ploar Accurex Plus";
                    break;
                case 4:
                    monitor = "Polar XTrainer Plus";
                    break;
                case 6:
                    monitor = "Polar S520";
                    break;
                case 7:
                    monitor = "Polar Coach";
                    break;
                case 8:
                    monitor = "Polar S210";
                    break;
                case 9:
                    monitor = "Polar S410";
                    break;
                case 10:
                    monitor = "Polar S510";
                    break;
                case 11:
                    monitor = "Polar S610 / S610i";
                    break;
                case 12:
                    monitor = "Polar S710 / S710i / S720i";
                    break;
                case 13:
                    monitor = "Polar S810 / S810i";
                    break;
                case 15:
                    monitor = "Polar E600";
                    break;
                case 20:
                    monitor = "Polar AXN500";
                    break;
                case 21:
                    monitor = "Polar AXN700";
                    break;
                case 22:
                    monitor = "Polar S625X / S725X";
                    break;
                case 23:
                    monitor = "Polar S725";
                    break;
                case 33:
                    monitor = "Polar CS400";
                    break;
                case 34:
                    monitor = "Polar CS600X";
                    break;
                case 35:
                    monitor = "Polar CS600";
                    break;
                case 36:
                    monitor = "Polar RS400";
                    break;
                case 37:
                    monitor = "Polar RS800";
                    break;
                case 38:
                    monitor = "Polar RS800X";
                    break;
            }
            return monitor;
        }
    }
}
