﻿namespace SEASSTEST
{
    partial class Form1
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.menuStrip1 = new System.Windows.Forms.MenuStrip();
            this.toolStripMenuItem1 = new System.Windows.Forms.ToolStripMenuItem();
            this.openFileToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.viewToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.generalParameterParamsCtrl1ToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.heartRateDataHRDataCtrl2ToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.generateToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.versionS = new System.Windows.Forms.Label();
            this.versionD = new System.Windows.Forms.TextBox();
            this.statusStrip1 = new System.Windows.Forms.StatusStrip();
            this.filename = new System.Windows.Forms.ToolStripStatusLabel();
            this.Parameter_panel = new System.Windows.Forms.Panel();
            this.label44 = new System.Windows.Forms.Label();
            this.label33 = new System.Windows.Forms.Label();
            this.panel1 = new System.Windows.Forms.Panel();
            this.distanceT = new System.Windows.Forms.Label();
            this.altMaxT = new System.Windows.Forms.Label();
            this.speedAvgT = new System.Windows.Forms.Label();
            this.speedMaxT = new System.Windows.Forms.Label();
            this.altAvgT = new System.Windows.Forms.Label();
            this.label22 = new System.Windows.Forms.Label();
            this.label21 = new System.Windows.Forms.Label();
            this.label19 = new System.Windows.Forms.Label();
            this.label20 = new System.Windows.Forms.Label();
            this.label17 = new System.Windows.Forms.Label();
            this.label10 = new System.Windows.Forms.Label();
            this.altitudeMaxD = new System.Windows.Forms.TextBox();
            this.HRminD = new System.Windows.Forms.TextBox();
            this.powerAvgD = new System.Windows.Forms.TextBox();
            this.altitudeAvgD = new System.Windows.Forms.TextBox();
            this.HRmaxD = new System.Windows.Forms.TextBox();
            this.powerMaxD = new System.Windows.Forms.TextBox();
            this.speedMaxD = new System.Windows.Forms.TextBox();
            this.speedAvgD = new System.Windows.Forms.TextBox();
            this.HRavgD = new System.Windows.Forms.TextBox();
            this.distanceD = new System.Windows.Forms.TextBox();
            this.label36 = new System.Windows.Forms.Label();
            this.label34 = new System.Windows.Forms.Label();
            this.label32 = new System.Windows.Forms.Label();
            this.label31 = new System.Windows.Forms.Label();
            this.label30 = new System.Windows.Forms.Label();
            this.label29 = new System.Windows.Forms.Label();
            this.label28 = new System.Windows.Forms.Label();
            this.label27 = new System.Windows.Forms.Label();
            this.label26 = new System.Windows.Forms.Label();
            this.label25 = new System.Windows.Forms.Label();
            this.label24 = new System.Windows.Forms.Label();
            this.label18 = new System.Windows.Forms.Label();
            this.restHRD = new System.Windows.Forms.TextBox();
            this.startDelayD = new System.Windows.Forms.TextBox();
            this.vo2maxD = new System.Windows.Forms.TextBox();
            this.weightD = new System.Windows.Forms.TextBox();
            this.time3D = new System.Windows.Forms.TextBox();
            this.time2D = new System.Windows.Forms.TextBox();
            this.activeLimitD = new System.Windows.Forms.TextBox();
            this.maxHRD = new System.Windows.Forms.TextBox();
            this.lower2D = new System.Windows.Forms.TextBox();
            this.upper3D = new System.Windows.Forms.TextBox();
            this.lower3D = new System.Windows.Forms.TextBox();
            this.time1D = new System.Windows.Forms.TextBox();
            this.upper2D = new System.Windows.Forms.TextBox();
            this.lower1D = new System.Windows.Forms.TextBox();
            this.upper1D = new System.Windows.Forms.TextBox();
            this.label16 = new System.Windows.Forms.Label();
            this.label15 = new System.Windows.Forms.Label();
            this.label14 = new System.Windows.Forms.Label();
            this.label13 = new System.Windows.Forms.Label();
            this.label12 = new System.Windows.Forms.Label();
            this.label11 = new System.Windows.Forms.Label();
            this.intervalD = new System.Windows.Forms.TextBox();
            this.intervalS = new System.Windows.Forms.Label();
            this.label9 = new System.Windows.Forms.Label();
            this.label8 = new System.Windows.Forms.Label();
            this.label7 = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.lengthD = new System.Windows.Forms.TextBox();
            this.dateD = new System.Windows.Forms.TextBox();
            this.startTimeD = new System.Windows.Forms.TextBox();
            this.monitorD = new System.Windows.Forms.TextBox();
            this.lengthS = new System.Windows.Forms.Label();
            this.startTimeS = new System.Windows.Forms.Label();
            this.dateS = new System.Windows.Forms.Label();
            this.monitorS = new System.Windows.Forms.Label();
            this.HRData_Panel = new System.Windows.Forms.Panel();
            this.ccT = new System.Windows.Forms.TextBox();
            this.usOFF = new System.Windows.Forms.Button();
            this.cadenceOFF = new System.Windows.Forms.Button();
            this.altitudeOFF = new System.Windows.Forms.Button();
            this.powerOFF = new System.Windows.Forms.Button();
            this.power_balanceOFF = new System.Windows.Forms.Button();
            this.power_indexOFF = new System.Windows.Forms.Button();
            this.usON = new System.Windows.Forms.Button();
            this.power_indexON = new System.Windows.Forms.Button();
            this.power_balanceON = new System.Windows.Forms.Button();
            this.powerON = new System.Windows.Forms.Button();
            this.altitudeON = new System.Windows.Forms.Button();
            this.cadenceON = new System.Windows.Forms.Button();
            this.speedOFF = new System.Windows.Forms.Button();
            this.speedON = new System.Windows.Forms.Button();
            this.label43 = new System.Windows.Forms.Label();
            this.label42 = new System.Windows.Forms.Label();
            this.label41 = new System.Windows.Forms.Label();
            this.label40 = new System.Windows.Forms.Label();
            this.label39 = new System.Windows.Forms.Label();
            this.label38 = new System.Windows.Forms.Label();
            this.label37 = new System.Windows.Forms.Label();
            this.label35 = new System.Windows.Forms.Label();
            this.SMode = new System.Windows.Forms.Label();
            this.dataGridView1 = new System.Windows.Forms.DataGridView();
            this.chart_panel = new System.Windows.Forms.Panel();
            this.bitT4 = new System.Windows.Forms.Label();
            this.bitT3 = new System.Windows.Forms.Label();
            this.bitT2 = new System.Windows.Forms.Label();
            this.bitT1 = new System.Windows.Forms.Label();
            this.bit = new System.Windows.Forms.Label();
            this.label46 = new System.Windows.Forms.Label();
            this.label45 = new System.Windows.Forms.Label();
            this.label23 = new System.Windows.Forms.Label();
            this.power_chart = new System.Windows.Forms.Button();
            this.hr_chart = new System.Windows.Forms.Button();
            this.chart = new ZedGraph.ZedGraphControl();
            this.speed_chart = new System.Windows.Forms.Button();
            this.menuStrip1.SuspendLayout();
            this.statusStrip1.SuspendLayout();
            this.Parameter_panel.SuspendLayout();
            this.panel1.SuspendLayout();
            this.HRData_Panel.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView1)).BeginInit();
            this.chart_panel.SuspendLayout();
            this.SuspendLayout();
            // 
            // menuStrip1
            // 
            this.menuStrip1.BackColor = System.Drawing.SystemColors.ControlDarkDark;
            this.menuStrip1.ImageScalingSize = new System.Drawing.Size(24, 24);
            this.menuStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.toolStripMenuItem1,
            this.viewToolStripMenuItem});
            this.menuStrip1.Location = new System.Drawing.Point(0, 0);
            this.menuStrip1.Name = "menuStrip1";
            this.menuStrip1.Size = new System.Drawing.Size(784, 24);
            this.menuStrip1.TabIndex = 2;
            this.menuStrip1.Text = "menuStrip1";
            // 
            // toolStripMenuItem1
            // 
            this.toolStripMenuItem1.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.openFileToolStripMenuItem});
            this.toolStripMenuItem1.Name = "toolStripMenuItem1";
            this.toolStripMenuItem1.Size = new System.Drawing.Size(39, 20);
            this.toolStripMenuItem1.Text = "File";
            // 
            // openFileToolStripMenuItem
            // 
            this.openFileToolStripMenuItem.Name = "openFileToolStripMenuItem";
            this.openFileToolStripMenuItem.Size = new System.Drawing.Size(130, 22);
            this.openFileToolStripMenuItem.Text = "Open File";
            this.openFileToolStripMenuItem.Click += new System.EventHandler(this.openFileToolStripMenuItem_Click);
            // 
            // viewToolStripMenuItem
            // 
            this.viewToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.generalParameterParamsCtrl1ToolStripMenuItem,
            this.heartRateDataHRDataCtrl2ToolStripMenuItem,
            this.generateToolStripMenuItem});
            this.viewToolStripMenuItem.Name = "viewToolStripMenuItem";
            this.viewToolStripMenuItem.Size = new System.Drawing.Size(47, 20);
            this.viewToolStripMenuItem.Text = "View";
            // 
            // generalParameterParamsCtrl1ToolStripMenuItem
            // 
            this.generalParameterParamsCtrl1ToolStripMenuItem.Name = "generalParameterParamsCtrl1ToolStripMenuItem";
            this.generalParameterParamsCtrl1ToolStripMenuItem.Size = new System.Drawing.Size(304, 22);
            this.generalParameterParamsCtrl1ToolStripMenuItem.Text = "General Parameter [Params]          Ctrl + 1";
            this.generalParameterParamsCtrl1ToolStripMenuItem.Click += new System.EventHandler(this.generalParameterParamsCtrl1ToolStripMenuItem_Click);
            // 
            // heartRateDataHRDataCtrl2ToolStripMenuItem
            // 
            this.heartRateDataHRDataCtrl2ToolStripMenuItem.Name = "heartRateDataHRDataCtrl2ToolStripMenuItem";
            this.heartRateDataHRDataCtrl2ToolStripMenuItem.Size = new System.Drawing.Size(304, 22);
            this.heartRateDataHRDataCtrl2ToolStripMenuItem.Text = "Heart Rate Data [HRData]              Ctrl + 2";
            this.heartRateDataHRDataCtrl2ToolStripMenuItem.Click += new System.EventHandler(this.heartRateDataHRDataCtrl2ToolStripMenuItem_Click);
            // 
            // generateToolStripMenuItem
            // 
            this.generateToolStripMenuItem.Name = "generateToolStripMenuItem";
            this.generateToolStripMenuItem.Size = new System.Drawing.Size(304, 22);
            this.generateToolStripMenuItem.Text = "Generate Chart                                Ctrl + 3";
            this.generateToolStripMenuItem.Click += new System.EventHandler(this.generateToolStripMenuItem_Click);
            // 
            // versionS
            // 
            this.versionS.AutoSize = true;
            this.versionS.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.versionS.Location = new System.Drawing.Point(87, 20);
            this.versionS.Name = "versionS";
            this.versionS.Size = new System.Drawing.Size(85, 20);
            this.versionS.TabIndex = 3;
            this.versionS.Text = "Version : ";
            // 
            // versionD
            // 
            this.versionD.Location = new System.Drawing.Point(173, 24);
            this.versionD.Name = "versionD";
            this.versionD.ReadOnly = true;
            this.versionD.Size = new System.Drawing.Size(100, 22);
            this.versionD.TabIndex = 4;
            // 
            // statusStrip1
            // 
            this.statusStrip1.ImageScalingSize = new System.Drawing.Size(24, 24);
            this.statusStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.filename});
            this.statusStrip1.Location = new System.Drawing.Point(0, 540);
            this.statusStrip1.Name = "statusStrip1";
            this.statusStrip1.Size = new System.Drawing.Size(784, 22);
            this.statusStrip1.TabIndex = 5;
            this.statusStrip1.Text = "statusStrip1";
            // 
            // filename
            // 
            this.filename.Name = "filename";
            this.filename.Size = new System.Drawing.Size(0, 17);
            // 
            // Parameter_panel
            // 
            this.Parameter_panel.Controls.Add(this.label44);
            this.Parameter_panel.Controls.Add(this.label33);
            this.Parameter_panel.Controls.Add(this.panel1);
            this.Parameter_panel.Controls.Add(this.label24);
            this.Parameter_panel.Controls.Add(this.label18);
            this.Parameter_panel.Controls.Add(this.restHRD);
            this.Parameter_panel.Controls.Add(this.startDelayD);
            this.Parameter_panel.Controls.Add(this.vo2maxD);
            this.Parameter_panel.Controls.Add(this.weightD);
            this.Parameter_panel.Controls.Add(this.time3D);
            this.Parameter_panel.Controls.Add(this.time2D);
            this.Parameter_panel.Controls.Add(this.activeLimitD);
            this.Parameter_panel.Controls.Add(this.maxHRD);
            this.Parameter_panel.Controls.Add(this.lower2D);
            this.Parameter_panel.Controls.Add(this.upper3D);
            this.Parameter_panel.Controls.Add(this.lower3D);
            this.Parameter_panel.Controls.Add(this.time1D);
            this.Parameter_panel.Controls.Add(this.upper2D);
            this.Parameter_panel.Controls.Add(this.lower1D);
            this.Parameter_panel.Controls.Add(this.upper1D);
            this.Parameter_panel.Controls.Add(this.label16);
            this.Parameter_panel.Controls.Add(this.label15);
            this.Parameter_panel.Controls.Add(this.label14);
            this.Parameter_panel.Controls.Add(this.label13);
            this.Parameter_panel.Controls.Add(this.label12);
            this.Parameter_panel.Controls.Add(this.label11);
            this.Parameter_panel.Controls.Add(this.intervalD);
            this.Parameter_panel.Controls.Add(this.intervalS);
            this.Parameter_panel.Controls.Add(this.label9);
            this.Parameter_panel.Controls.Add(this.label8);
            this.Parameter_panel.Controls.Add(this.label7);
            this.Parameter_panel.Controls.Add(this.label6);
            this.Parameter_panel.Controls.Add(this.label5);
            this.Parameter_panel.Controls.Add(this.label4);
            this.Parameter_panel.Controls.Add(this.label3);
            this.Parameter_panel.Controls.Add(this.label2);
            this.Parameter_panel.Controls.Add(this.label1);
            this.Parameter_panel.Controls.Add(this.lengthD);
            this.Parameter_panel.Controls.Add(this.dateD);
            this.Parameter_panel.Controls.Add(this.startTimeD);
            this.Parameter_panel.Controls.Add(this.monitorD);
            this.Parameter_panel.Controls.Add(this.lengthS);
            this.Parameter_panel.Controls.Add(this.startTimeS);
            this.Parameter_panel.Controls.Add(this.dateS);
            this.Parameter_panel.Controls.Add(this.monitorS);
            this.Parameter_panel.Controls.Add(this.versionS);
            this.Parameter_panel.Controls.Add(this.versionD);
            this.Parameter_panel.Location = new System.Drawing.Point(0, 41);
            this.Parameter_panel.Name = "Parameter_panel";
            this.Parameter_panel.Size = new System.Drawing.Size(784, 496);
            this.Parameter_panel.TabIndex = 6;
            this.Parameter_panel.Visible = false;
            // 
            // label44
            // 
            this.label44.AutoSize = true;
            this.label44.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.label44.Location = new System.Drawing.Point(698, 423);
            this.label44.Name = "label44";
            this.label44.Size = new System.Drawing.Size(76, 17);
            this.label44.TabIndex = 55;
            this.label44.Text = "ml/min/kg";
            // 
            // label33
            // 
            this.label33.AutoSize = true;
            this.label33.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.label33.Location = new System.Drawing.Point(718, 450);
            this.label33.Name = "label33";
            this.label33.Size = new System.Drawing.Size(30, 20);
            this.label33.TabIndex = 54;
            this.label33.Text = "Kg";
            // 
            // panel1
            // 
            this.panel1.BackColor = System.Drawing.Color.Silver;
            this.panel1.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.panel1.Controls.Add(this.distanceT);
            this.panel1.Controls.Add(this.altMaxT);
            this.panel1.Controls.Add(this.speedAvgT);
            this.panel1.Controls.Add(this.speedMaxT);
            this.panel1.Controls.Add(this.altAvgT);
            this.panel1.Controls.Add(this.label22);
            this.panel1.Controls.Add(this.label21);
            this.panel1.Controls.Add(this.label19);
            this.panel1.Controls.Add(this.label20);
            this.panel1.Controls.Add(this.label17);
            this.panel1.Controls.Add(this.label10);
            this.panel1.Controls.Add(this.altitudeMaxD);
            this.panel1.Controls.Add(this.HRminD);
            this.panel1.Controls.Add(this.powerAvgD);
            this.panel1.Controls.Add(this.altitudeAvgD);
            this.panel1.Controls.Add(this.HRmaxD);
            this.panel1.Controls.Add(this.powerMaxD);
            this.panel1.Controls.Add(this.speedMaxD);
            this.panel1.Controls.Add(this.speedAvgD);
            this.panel1.Controls.Add(this.HRavgD);
            this.panel1.Controls.Add(this.distanceD);
            this.panel1.Controls.Add(this.label36);
            this.panel1.Controls.Add(this.label34);
            this.panel1.Controls.Add(this.label32);
            this.panel1.Controls.Add(this.label31);
            this.panel1.Controls.Add(this.label30);
            this.panel1.Controls.Add(this.label29);
            this.panel1.Controls.Add(this.label28);
            this.panel1.Controls.Add(this.label27);
            this.panel1.Controls.Add(this.label26);
            this.panel1.Controls.Add(this.label25);
            this.panel1.Location = new System.Drawing.Point(12, 222);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(452, 266);
            this.panel1.TabIndex = 53;
            // 
            // distanceT
            // 
            this.distanceT.AutoSize = true;
            this.distanceT.Font = new System.Drawing.Font("新細明體", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.distanceT.Location = new System.Drawing.Point(196, 100);
            this.distanceT.Name = "distanceT";
            this.distanceT.Size = new System.Drawing.Size(0, 12);
            this.distanceT.TabIndex = 65;
            // 
            // altMaxT
            // 
            this.altMaxT.AutoSize = true;
            this.altMaxT.Font = new System.Drawing.Font("新細明體", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.altMaxT.Location = new System.Drawing.Point(411, 142);
            this.altMaxT.Name = "altMaxT";
            this.altMaxT.Size = new System.Drawing.Size(0, 12);
            this.altMaxT.TabIndex = 64;
            // 
            // speedAvgT
            // 
            this.speedAvgT.AutoSize = true;
            this.speedAvgT.Font = new System.Drawing.Font("新細明體", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.speedAvgT.Location = new System.Drawing.Point(411, 180);
            this.speedAvgT.Name = "speedAvgT";
            this.speedAvgT.Size = new System.Drawing.Size(0, 12);
            this.speedAvgT.TabIndex = 63;
            // 
            // speedMaxT
            // 
            this.speedMaxT.AutoSize = true;
            this.speedMaxT.Font = new System.Drawing.Font("新細明體", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.speedMaxT.Location = new System.Drawing.Point(411, 220);
            this.speedMaxT.Name = "speedMaxT";
            this.speedMaxT.Size = new System.Drawing.Size(0, 12);
            this.speedMaxT.TabIndex = 62;
            // 
            // altAvgT
            // 
            this.altAvgT.AutoSize = true;
            this.altAvgT.Font = new System.Drawing.Font("新細明體", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.altAvgT.Location = new System.Drawing.Point(411, 100);
            this.altAvgT.Name = "altAvgT";
            this.altAvgT.Size = new System.Drawing.Size(0, 12);
            this.altAvgT.TabIndex = 61;
            // 
            // label22
            // 
            this.label22.AutoSize = true;
            this.label22.Font = new System.Drawing.Font("新細明體", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.label22.Location = new System.Drawing.Point(196, 183);
            this.label22.Name = "label22";
            this.label22.Size = new System.Drawing.Size(29, 12);
            this.label22.TabIndex = 60;
            this.label22.Text = "bpm";
            // 
            // label21
            // 
            this.label21.AutoSize = true;
            this.label21.Font = new System.Drawing.Font("新細明體", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.label21.Location = new System.Drawing.Point(196, 142);
            this.label21.Name = "label21";
            this.label21.Size = new System.Drawing.Size(29, 12);
            this.label21.TabIndex = 59;
            this.label21.Text = "bpm";
            // 
            // label19
            // 
            this.label19.AutoSize = true;
            this.label19.Font = new System.Drawing.Font("新細明體", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.label19.Location = new System.Drawing.Point(196, 223);
            this.label19.Name = "label19";
            this.label19.Size = new System.Drawing.Size(29, 12);
            this.label19.TabIndex = 58;
            this.label19.Text = "bpm";
            // 
            // label20
            // 
            this.label20.AutoSize = true;
            this.label20.Font = new System.Drawing.Font("新細明體", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.label20.Location = new System.Drawing.Point(411, 64);
            this.label20.Name = "label20";
            this.label20.Size = new System.Drawing.Size(36, 12);
            this.label20.TabIndex = 57;
            this.label20.Text = "Watts";
            // 
            // label17
            // 
            this.label17.AutoSize = true;
            this.label17.Font = new System.Drawing.Font("新細明體", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.label17.Location = new System.Drawing.Point(411, 22);
            this.label17.Name = "label17";
            this.label17.Size = new System.Drawing.Size(36, 12);
            this.label17.TabIndex = 55;
            this.label17.Text = "Watts";
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Font = new System.Drawing.Font("Microsoft Sans Serif", 16F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.label10.Location = new System.Drawing.Point(19, 29);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(192, 26);
            this.label10.TabIndex = 54;
            this.label10.Text = "Summary Data : ";
            // 
            // altitudeMaxD
            // 
            this.altitudeMaxD.Location = new System.Drawing.Point(335, 137);
            this.altitudeMaxD.Name = "altitudeMaxD";
            this.altitudeMaxD.ReadOnly = true;
            this.altitudeMaxD.Size = new System.Drawing.Size(70, 22);
            this.altitudeMaxD.TabIndex = 23;
            // 
            // HRminD
            // 
            this.HRminD.Location = new System.Drawing.Point(120, 217);
            this.HRminD.Name = "HRminD";
            this.HRminD.ReadOnly = true;
            this.HRminD.Size = new System.Drawing.Size(70, 22);
            this.HRminD.TabIndex = 21;
            // 
            // powerAvgD
            // 
            this.powerAvgD.Location = new System.Drawing.Point(335, 17);
            this.powerAvgD.Name = "powerAvgD";
            this.powerAvgD.ReadOnly = true;
            this.powerAvgD.Size = new System.Drawing.Size(70, 22);
            this.powerAvgD.TabIndex = 20;
            // 
            // altitudeAvgD
            // 
            this.altitudeAvgD.Location = new System.Drawing.Point(335, 97);
            this.altitudeAvgD.Name = "altitudeAvgD";
            this.altitudeAvgD.ReadOnly = true;
            this.altitudeAvgD.Size = new System.Drawing.Size(70, 22);
            this.altitudeAvgD.TabIndex = 19;
            // 
            // HRmaxD
            // 
            this.HRmaxD.Location = new System.Drawing.Point(120, 177);
            this.HRmaxD.Name = "HRmaxD";
            this.HRmaxD.ReadOnly = true;
            this.HRmaxD.Size = new System.Drawing.Size(70, 22);
            this.HRmaxD.TabIndex = 18;
            // 
            // powerMaxD
            // 
            this.powerMaxD.Location = new System.Drawing.Point(335, 57);
            this.powerMaxD.Name = "powerMaxD";
            this.powerMaxD.ReadOnly = true;
            this.powerMaxD.Size = new System.Drawing.Size(70, 22);
            this.powerMaxD.TabIndex = 16;
            // 
            // speedMaxD
            // 
            this.speedMaxD.Location = new System.Drawing.Point(335, 217);
            this.speedMaxD.Name = "speedMaxD";
            this.speedMaxD.ReadOnly = true;
            this.speedMaxD.Size = new System.Drawing.Size(70, 22);
            this.speedMaxD.TabIndex = 15;
            // 
            // speedAvgD
            // 
            this.speedAvgD.Location = new System.Drawing.Point(335, 177);
            this.speedAvgD.Name = "speedAvgD";
            this.speedAvgD.ReadOnly = true;
            this.speedAvgD.Size = new System.Drawing.Size(70, 22);
            this.speedAvgD.TabIndex = 14;
            // 
            // HRavgD
            // 
            this.HRavgD.Location = new System.Drawing.Point(120, 137);
            this.HRavgD.Name = "HRavgD";
            this.HRavgD.ReadOnly = true;
            this.HRavgD.Size = new System.Drawing.Size(70, 22);
            this.HRavgD.TabIndex = 13;
            // 
            // distanceD
            // 
            this.distanceD.Location = new System.Drawing.Point(120, 97);
            this.distanceD.Name = "distanceD";
            this.distanceD.ReadOnly = true;
            this.distanceD.Size = new System.Drawing.Size(70, 22);
            this.distanceD.TabIndex = 12;
            // 
            // label36
            // 
            this.label36.AutoSize = true;
            this.label36.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.label36.Location = new System.Drawing.Point(241, 97);
            this.label36.Name = "label36";
            this.label36.Size = new System.Drawing.Size(93, 15);
            this.label36.TabIndex = 11;
            this.label36.Text = "Altitude Avg : ";
            // 
            // label34
            // 
            this.label34.AutoSize = true;
            this.label34.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.label34.Location = new System.Drawing.Point(236, 137);
            this.label34.Name = "label34";
            this.label34.Size = new System.Drawing.Size(98, 15);
            this.label34.TabIndex = 9;
            this.label34.Text = "Altitude Max : ";
            // 
            // label32
            // 
            this.label32.AutoSize = true;
            this.label32.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.label32.Location = new System.Drawing.Point(244, 61);
            this.label32.Name = "label32";
            this.label32.Size = new System.Drawing.Size(90, 15);
            this.label32.TabIndex = 7;
            this.label32.Text = "Power Max : ";
            // 
            // label31
            // 
            this.label31.AutoSize = true;
            this.label31.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.label31.Location = new System.Drawing.Point(249, 19);
            this.label31.Name = "label31";
            this.label31.Size = new System.Drawing.Size(85, 15);
            this.label31.TabIndex = 6;
            this.label31.Text = "Power Avg : ";
            // 
            // label30
            // 
            this.label30.AutoSize = true;
            this.label30.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.label30.Location = new System.Drawing.Point(1, 180);
            this.label30.Name = "label30";
            this.label30.Size = new System.Drawing.Size(119, 15);
            this.label30.TabIndex = 5;
            this.label30.Text = "Heart Rate Max : ";
            // 
            // label29
            // 
            this.label29.AutoSize = true;
            this.label29.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.label29.Location = new System.Drawing.Point(3, 220);
            this.label29.Name = "label29";
            this.label29.Size = new System.Drawing.Size(116, 15);
            this.label29.TabIndex = 4;
            this.label29.Text = "Heart Rate Min : ";
            // 
            // label28
            // 
            this.label28.AutoSize = true;
            this.label28.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.label28.Location = new System.Drawing.Point(5, 139);
            this.label28.Name = "label28";
            this.label28.Size = new System.Drawing.Size(114, 15);
            this.label28.TabIndex = 3;
            this.label28.Text = "Heart Rate Avg : ";
            // 
            // label27
            // 
            this.label27.AutoSize = true;
            this.label27.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.label27.Location = new System.Drawing.Point(243, 220);
            this.label27.Name = "label27";
            this.label27.Size = new System.Drawing.Size(91, 15);
            this.label27.TabIndex = 2;
            this.label27.Text = "Speed Max : ";
            // 
            // label26
            // 
            this.label26.AutoSize = true;
            this.label26.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.label26.Location = new System.Drawing.Point(248, 180);
            this.label26.Name = "label26";
            this.label26.Size = new System.Drawing.Size(86, 15);
            this.label26.TabIndex = 1;
            this.label26.Text = "Speed Avg : ";
            // 
            // label25
            // 
            this.label25.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.label25.Location = new System.Drawing.Point(5, 97);
            this.label25.Name = "label25";
            this.label25.Size = new System.Drawing.Size(111, 40);
            this.label25.TabIndex = 0;
            this.label25.Text = "Total Distance Covered : ";
            // 
            // label24
            // 
            this.label24.AutoSize = true;
            this.label24.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.label24.Location = new System.Drawing.Point(718, 360);
            this.label24.Name = "label24";
            this.label24.Size = new System.Drawing.Size(55, 20);
            this.label24.TabIndex = 52;
            this.label24.Text = "(bpm)";
            // 
            // label18
            // 
            this.label18.AutoSize = true;
            this.label18.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.label18.Location = new System.Drawing.Point(718, 330);
            this.label18.Name = "label18";
            this.label18.Size = new System.Drawing.Size(55, 20);
            this.label18.TabIndex = 46;
            this.label18.Text = "(bpm)";
            // 
            // restHRD
            // 
            this.restHRD.Location = new System.Drawing.Point(612, 360);
            this.restHRD.Name = "restHRD";
            this.restHRD.ReadOnly = true;
            this.restHRD.Size = new System.Drawing.Size(100, 22);
            this.restHRD.TabIndex = 44;
            // 
            // startDelayD
            // 
            this.startDelayD.Location = new System.Drawing.Point(612, 390);
            this.startDelayD.Name = "startDelayD";
            this.startDelayD.ReadOnly = true;
            this.startDelayD.Size = new System.Drawing.Size(100, 22);
            this.startDelayD.TabIndex = 43;
            // 
            // vo2maxD
            // 
            this.vo2maxD.Location = new System.Drawing.Point(612, 420);
            this.vo2maxD.Name = "vo2maxD";
            this.vo2maxD.ReadOnly = true;
            this.vo2maxD.Size = new System.Drawing.Size(80, 22);
            this.vo2maxD.TabIndex = 42;
            // 
            // weightD
            // 
            this.weightD.Location = new System.Drawing.Point(612, 450);
            this.weightD.Name = "weightD";
            this.weightD.ReadOnly = true;
            this.weightD.Size = new System.Drawing.Size(100, 22);
            this.weightD.TabIndex = 41;
            // 
            // time3D
            // 
            this.time3D.Location = new System.Drawing.Point(612, 270);
            this.time3D.Name = "time3D";
            this.time3D.ReadOnly = true;
            this.time3D.Size = new System.Drawing.Size(100, 22);
            this.time3D.TabIndex = 40;
            // 
            // time2D
            // 
            this.time2D.Location = new System.Drawing.Point(612, 240);
            this.time2D.Name = "time2D";
            this.time2D.ReadOnly = true;
            this.time2D.Size = new System.Drawing.Size(100, 22);
            this.time2D.TabIndex = 39;
            // 
            // activeLimitD
            // 
            this.activeLimitD.Location = new System.Drawing.Point(612, 300);
            this.activeLimitD.Name = "activeLimitD";
            this.activeLimitD.ReadOnly = true;
            this.activeLimitD.Size = new System.Drawing.Size(100, 22);
            this.activeLimitD.TabIndex = 38;
            // 
            // maxHRD
            // 
            this.maxHRD.Location = new System.Drawing.Point(612, 330);
            this.maxHRD.Name = "maxHRD";
            this.maxHRD.ReadOnly = true;
            this.maxHRD.Size = new System.Drawing.Size(100, 22);
            this.maxHRD.TabIndex = 37;
            // 
            // lower2D
            // 
            this.lower2D.Location = new System.Drawing.Point(612, 120);
            this.lower2D.Name = "lower2D";
            this.lower2D.ReadOnly = true;
            this.lower2D.Size = new System.Drawing.Size(100, 22);
            this.lower2D.TabIndex = 36;
            // 
            // upper3D
            // 
            this.upper3D.Location = new System.Drawing.Point(612, 150);
            this.upper3D.Name = "upper3D";
            this.upper3D.ReadOnly = true;
            this.upper3D.Size = new System.Drawing.Size(100, 22);
            this.upper3D.TabIndex = 35;
            // 
            // lower3D
            // 
            this.lower3D.Location = new System.Drawing.Point(612, 180);
            this.lower3D.Name = "lower3D";
            this.lower3D.ReadOnly = true;
            this.lower3D.Size = new System.Drawing.Size(100, 22);
            this.lower3D.TabIndex = 34;
            // 
            // time1D
            // 
            this.time1D.Location = new System.Drawing.Point(612, 210);
            this.time1D.Name = "time1D";
            this.time1D.ReadOnly = true;
            this.time1D.Size = new System.Drawing.Size(100, 22);
            this.time1D.TabIndex = 33;
            // 
            // upper2D
            // 
            this.upper2D.Location = new System.Drawing.Point(612, 90);
            this.upper2D.Name = "upper2D";
            this.upper2D.ReadOnly = true;
            this.upper2D.Size = new System.Drawing.Size(100, 22);
            this.upper2D.TabIndex = 32;
            // 
            // lower1D
            // 
            this.lower1D.Location = new System.Drawing.Point(612, 60);
            this.lower1D.Name = "lower1D";
            this.lower1D.ReadOnly = true;
            this.lower1D.Size = new System.Drawing.Size(100, 22);
            this.lower1D.TabIndex = 31;
            // 
            // upper1D
            // 
            this.upper1D.Location = new System.Drawing.Point(612, 30);
            this.upper1D.Name = "upper1D";
            this.upper1D.ReadOnly = true;
            this.upper1D.Size = new System.Drawing.Size(100, 22);
            this.upper1D.TabIndex = 30;
            // 
            // label16
            // 
            this.label16.AutoSize = true;
            this.label16.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.label16.Location = new System.Drawing.Point(540, 450);
            this.label16.Name = "label16";
            this.label16.Size = new System.Drawing.Size(80, 20);
            this.label16.TabIndex = 29;
            this.label16.Text = "Weight : ";
            // 
            // label15
            // 
            this.label15.AutoSize = true;
            this.label15.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.label15.Location = new System.Drawing.Point(528, 420);
            this.label15.Name = "label15";
            this.label15.Size = new System.Drawing.Size(91, 20);
            this.label15.TabIndex = 28;
            this.label15.Text = "VO2max : ";
            // 
            // label14
            // 
            this.label14.AutoSize = true;
            this.label14.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.label14.Location = new System.Drawing.Point(516, 390);
            this.label14.Name = "label14";
            this.label14.Size = new System.Drawing.Size(109, 20);
            this.label14.TabIndex = 27;
            this.label14.Text = "StartDelay : ";
            // 
            // label13
            // 
            this.label13.AutoSize = true;
            this.label13.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.label13.Location = new System.Drawing.Point(537, 360);
            this.label13.Name = "label13";
            this.label13.Size = new System.Drawing.Size(88, 20);
            this.label13.TabIndex = 26;
            this.label13.Text = "RestHR : ";
            // 
            // label12
            // 
            this.label12.AutoSize = true;
            this.label12.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.label12.Location = new System.Drawing.Point(537, 330);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(82, 20);
            this.label12.TabIndex = 25;
            this.label12.Text = "MaxHR : ";
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.label11.Location = new System.Drawing.Point(506, 300);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(111, 20);
            this.label11.TabIndex = 24;
            this.label11.Text = "ActiveLimit : ";
            // 
            // intervalD
            // 
            this.intervalD.Location = new System.Drawing.Point(173, 170);
            this.intervalD.Name = "intervalD";
            this.intervalD.ReadOnly = true;
            this.intervalD.Size = new System.Drawing.Size(100, 22);
            this.intervalD.TabIndex = 23;
            // 
            // intervalS
            // 
            this.intervalS.AutoSize = true;
            this.intervalS.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.intervalS.Location = new System.Drawing.Point(90, 170);
            this.intervalS.Name = "intervalS";
            this.intervalS.Size = new System.Drawing.Size(84, 20);
            this.intervalS.TabIndex = 22;
            this.intervalS.Text = "Interval : ";
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.label9.Location = new System.Drawing.Point(470, 270);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(156, 20);
            this.label9.TabIndex = 21;
            this.label9.Text = "Exercise Timer 3 : ";
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.label8.Location = new System.Drawing.Point(469, 240);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(156, 20);
            this.label8.TabIndex = 20;
            this.label8.Text = "Exercise Timer 2 : ";
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.label7.Location = new System.Drawing.Point(469, 210);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(156, 20);
            this.label7.TabIndex = 19;
            this.label7.Text = "Exercise Timer 1 : ";
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.label6.Location = new System.Drawing.Point(532, 180);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(87, 20);
            this.label6.TabIndex = 18;
            this.label6.Text = "Lower 3 : ";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.label5.Location = new System.Drawing.Point(533, 150);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(88, 20);
            this.label5.TabIndex = 17;
            this.label5.Text = "Upper 3 : ";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.label4.Location = new System.Drawing.Point(532, 120);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(87, 20);
            this.label4.TabIndex = 16;
            this.label4.Text = "Lower 2 : ";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.label3.Location = new System.Drawing.Point(533, 90);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(88, 20);
            this.label3.TabIndex = 15;
            this.label3.Text = "Upper 2 : ";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.label2.Location = new System.Drawing.Point(532, 60);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(87, 20);
            this.label2.TabIndex = 14;
            this.label2.Text = "Lower 1 : ";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.label1.Location = new System.Drawing.Point(531, 30);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(88, 20);
            this.label1.TabIndex = 13;
            this.label1.Text = "Upper 1 : ";
            // 
            // lengthD
            // 
            this.lengthD.Location = new System.Drawing.Point(173, 140);
            this.lengthD.Name = "lengthD";
            this.lengthD.ReadOnly = true;
            this.lengthD.Size = new System.Drawing.Size(100, 22);
            this.lengthD.TabIndex = 12;
            // 
            // dateD
            // 
            this.dateD.Location = new System.Drawing.Point(173, 80);
            this.dateD.Name = "dateD";
            this.dateD.ReadOnly = true;
            this.dateD.Size = new System.Drawing.Size(100, 22);
            this.dateD.TabIndex = 11;
            // 
            // startTimeD
            // 
            this.startTimeD.Location = new System.Drawing.Point(173, 110);
            this.startTimeD.Name = "startTimeD";
            this.startTimeD.ReadOnly = true;
            this.startTimeD.Size = new System.Drawing.Size(100, 22);
            this.startTimeD.TabIndex = 10;
            // 
            // monitorD
            // 
            this.monitorD.Location = new System.Drawing.Point(173, 50);
            this.monitorD.Name = "monitorD";
            this.monitorD.ReadOnly = true;
            this.monitorD.Size = new System.Drawing.Size(100, 22);
            this.monitorD.TabIndex = 9;
            // 
            // lengthS
            // 
            this.lengthS.AutoSize = true;
            this.lengthS.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.lengthS.Location = new System.Drawing.Point(93, 137);
            this.lengthS.Name = "lengthS";
            this.lengthS.Size = new System.Drawing.Size(80, 20);
            this.lengthS.TabIndex = 8;
            this.lengthS.Text = "Length : ";
            // 
            // startTimeS
            // 
            this.startTimeS.AutoSize = true;
            this.startTimeS.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.startTimeS.Location = new System.Drawing.Point(67, 107);
            this.startTimeS.Name = "startTimeS";
            this.startTimeS.Size = new System.Drawing.Size(107, 20);
            this.startTimeS.TabIndex = 7;
            this.startTimeS.Text = "Start Time : ";
            // 
            // dateS
            // 
            this.dateS.AutoSize = true;
            this.dateS.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.dateS.Location = new System.Drawing.Point(110, 77);
            this.dateS.Name = "dateS";
            this.dateS.Size = new System.Drawing.Size(63, 20);
            this.dateS.TabIndex = 6;
            this.dateS.Text = "Date : ";
            // 
            // monitorS
            // 
            this.monitorS.AutoSize = true;
            this.monitorS.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.monitorS.Location = new System.Drawing.Point(89, 50);
            this.monitorS.Name = "monitorS";
            this.monitorS.Size = new System.Drawing.Size(84, 20);
            this.monitorS.TabIndex = 5;
            this.monitorS.Text = "Monitor : ";
            // 
            // HRData_Panel
            // 
            this.HRData_Panel.Controls.Add(this.ccT);
            this.HRData_Panel.Controls.Add(this.usOFF);
            this.HRData_Panel.Controls.Add(this.cadenceOFF);
            this.HRData_Panel.Controls.Add(this.altitudeOFF);
            this.HRData_Panel.Controls.Add(this.powerOFF);
            this.HRData_Panel.Controls.Add(this.power_balanceOFF);
            this.HRData_Panel.Controls.Add(this.power_indexOFF);
            this.HRData_Panel.Controls.Add(this.usON);
            this.HRData_Panel.Controls.Add(this.power_indexON);
            this.HRData_Panel.Controls.Add(this.power_balanceON);
            this.HRData_Panel.Controls.Add(this.powerON);
            this.HRData_Panel.Controls.Add(this.altitudeON);
            this.HRData_Panel.Controls.Add(this.cadenceON);
            this.HRData_Panel.Controls.Add(this.speedOFF);
            this.HRData_Panel.Controls.Add(this.speedON);
            this.HRData_Panel.Controls.Add(this.label43);
            this.HRData_Panel.Controls.Add(this.label42);
            this.HRData_Panel.Controls.Add(this.label41);
            this.HRData_Panel.Controls.Add(this.label40);
            this.HRData_Panel.Controls.Add(this.label39);
            this.HRData_Panel.Controls.Add(this.label38);
            this.HRData_Panel.Controls.Add(this.label37);
            this.HRData_Panel.Controls.Add(this.label35);
            this.HRData_Panel.Controls.Add(this.SMode);
            this.HRData_Panel.Controls.Add(this.dataGridView1);
            this.HRData_Panel.Location = new System.Drawing.Point(12, 41);
            this.HRData_Panel.Name = "HRData_Panel";
            this.HRData_Panel.Size = new System.Drawing.Size(772, 496);
            this.HRData_Panel.TabIndex = 7;
            this.HRData_Panel.Visible = false;
            // 
            // ccT
            // 
            this.ccT.Location = new System.Drawing.Point(658, 352);
            this.ccT.Name = "ccT";
            this.ccT.ReadOnly = true;
            this.ccT.Size = new System.Drawing.Size(100, 22);
            this.ccT.TabIndex = 26;
            // 
            // usOFF
            // 
            this.usOFF.Location = new System.Drawing.Point(706, 396);
            this.usOFF.Name = "usOFF";
            this.usOFF.Size = new System.Drawing.Size(41, 23);
            this.usOFF.TabIndex = 25;
            this.usOFF.Text = "Euro";
            this.usOFF.UseVisualStyleBackColor = true;
            this.usOFF.MouseClick += new System.Windows.Forms.MouseEventHandler(this.usoff);
            // 
            // cadenceOFF
            // 
            this.cadenceOFF.Location = new System.Drawing.Point(706, 88);
            this.cadenceOFF.Name = "cadenceOFF";
            this.cadenceOFF.Size = new System.Drawing.Size(41, 23);
            this.cadenceOFF.TabIndex = 24;
            this.cadenceOFF.Text = "OFF";
            this.cadenceOFF.UseVisualStyleBackColor = true;
            this.cadenceOFF.MouseClick += new System.Windows.Forms.MouseEventHandler(this.cadenceoff);
            // 
            // altitudeOFF
            // 
            this.altitudeOFF.Location = new System.Drawing.Point(706, 135);
            this.altitudeOFF.Name = "altitudeOFF";
            this.altitudeOFF.Size = new System.Drawing.Size(41, 23);
            this.altitudeOFF.TabIndex = 23;
            this.altitudeOFF.Text = "OFF";
            this.altitudeOFF.UseVisualStyleBackColor = true;
            this.altitudeOFF.MouseClick += new System.Windows.Forms.MouseEventHandler(this.altitudeoff);
            // 
            // powerOFF
            // 
            this.powerOFF.Location = new System.Drawing.Point(706, 188);
            this.powerOFF.Name = "powerOFF";
            this.powerOFF.Size = new System.Drawing.Size(41, 23);
            this.powerOFF.TabIndex = 22;
            this.powerOFF.Text = "OFF";
            this.powerOFF.UseVisualStyleBackColor = true;
            this.powerOFF.MouseClick += new System.Windows.Forms.MouseEventHandler(this.poweroff);
            // 
            // power_balanceOFF
            // 
            this.power_balanceOFF.Location = new System.Drawing.Point(706, 247);
            this.power_balanceOFF.Name = "power_balanceOFF";
            this.power_balanceOFF.Size = new System.Drawing.Size(41, 23);
            this.power_balanceOFF.TabIndex = 21;
            this.power_balanceOFF.Text = "OFF";
            this.power_balanceOFF.UseVisualStyleBackColor = true;
            this.power_balanceOFF.MouseClick += new System.Windows.Forms.MouseEventHandler(this.power_balanceoff);
            // 
            // power_indexOFF
            // 
            this.power_indexOFF.Location = new System.Drawing.Point(706, 295);
            this.power_indexOFF.Name = "power_indexOFF";
            this.power_indexOFF.Size = new System.Drawing.Size(41, 23);
            this.power_indexOFF.TabIndex = 20;
            this.power_indexOFF.Text = "OFF";
            this.power_indexOFF.UseVisualStyleBackColor = true;
            this.power_indexOFF.MouseClick += new System.Windows.Forms.MouseEventHandler(this.power_indexoff);
            // 
            // usON
            // 
            this.usON.Location = new System.Drawing.Point(658, 396);
            this.usON.Name = "usON";
            this.usON.Size = new System.Drawing.Size(41, 23);
            this.usON.TabIndex = 18;
            this.usON.Text = "US";
            this.usON.UseVisualStyleBackColor = true;
            this.usON.MouseClick += new System.Windows.Forms.MouseEventHandler(this.uson);
            // 
            // power_indexON
            // 
            this.power_indexON.Location = new System.Drawing.Point(659, 295);
            this.power_indexON.Name = "power_indexON";
            this.power_indexON.Size = new System.Drawing.Size(41, 23);
            this.power_indexON.TabIndex = 16;
            this.power_indexON.Text = "ON";
            this.power_indexON.UseVisualStyleBackColor = true;
            this.power_indexON.MouseClick += new System.Windows.Forms.MouseEventHandler(this.power_indexon);
            // 
            // power_balanceON
            // 
            this.power_balanceON.Location = new System.Drawing.Point(658, 247);
            this.power_balanceON.Name = "power_balanceON";
            this.power_balanceON.Size = new System.Drawing.Size(41, 23);
            this.power_balanceON.TabIndex = 15;
            this.power_balanceON.Text = "ON";
            this.power_balanceON.UseVisualStyleBackColor = true;
            this.power_balanceON.MouseClick += new System.Windows.Forms.MouseEventHandler(this.power_balanceon);
            // 
            // powerON
            // 
            this.powerON.Location = new System.Drawing.Point(658, 188);
            this.powerON.Name = "powerON";
            this.powerON.Size = new System.Drawing.Size(41, 23);
            this.powerON.TabIndex = 14;
            this.powerON.Text = "ON";
            this.powerON.UseVisualStyleBackColor = true;
            this.powerON.MouseClick += new System.Windows.Forms.MouseEventHandler(this.poweron);
            // 
            // altitudeON
            // 
            this.altitudeON.Location = new System.Drawing.Point(659, 135);
            this.altitudeON.Name = "altitudeON";
            this.altitudeON.Size = new System.Drawing.Size(41, 23);
            this.altitudeON.TabIndex = 13;
            this.altitudeON.Text = "ON";
            this.altitudeON.UseVisualStyleBackColor = true;
            this.altitudeON.MouseClick += new System.Windows.Forms.MouseEventHandler(this.altitudeon);
            // 
            // cadenceON
            // 
            this.cadenceON.Location = new System.Drawing.Point(659, 88);
            this.cadenceON.Name = "cadenceON";
            this.cadenceON.Size = new System.Drawing.Size(41, 23);
            this.cadenceON.TabIndex = 12;
            this.cadenceON.Text = "ON";
            this.cadenceON.UseVisualStyleBackColor = true;
            this.cadenceON.MouseClick += new System.Windows.Forms.MouseEventHandler(this.cadenceon);
            // 
            // speedOFF
            // 
            this.speedOFF.Location = new System.Drawing.Point(706, 48);
            this.speedOFF.Name = "speedOFF";
            this.speedOFF.Size = new System.Drawing.Size(41, 23);
            this.speedOFF.TabIndex = 11;
            this.speedOFF.Text = "OFF";
            this.speedOFF.UseVisualStyleBackColor = true;
            this.speedOFF.MouseClick += new System.Windows.Forms.MouseEventHandler(this.speedoff);
            // 
            // speedON
            // 
            this.speedON.Location = new System.Drawing.Point(658, 48);
            this.speedON.Name = "speedON";
            this.speedON.Size = new System.Drawing.Size(41, 23);
            this.speedON.TabIndex = 10;
            this.speedON.Text = "ON";
            this.speedON.UseVisualStyleBackColor = true;
            this.speedON.MouseClick += new System.Windows.Forms.MouseEventHandler(this.speedon);
            // 
            // label43
            // 
            this.label43.AutoSize = true;
            this.label43.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.label43.Location = new System.Drawing.Point(569, 90);
            this.label43.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label43.Name = "label43";
            this.label43.Size = new System.Drawing.Size(95, 20);
            this.label43.TabIndex = 9;
            this.label43.Text = "Cadence : ";
            // 
            // label42
            // 
            this.label42.AutoSize = true;
            this.label42.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.label42.Location = new System.Drawing.Point(572, 140);
            this.label42.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label42.Name = "label42";
            this.label42.Size = new System.Drawing.Size(86, 20);
            this.label42.TabIndex = 8;
            this.label42.Text = "Altitude : ";
            // 
            // label41
            // 
            this.label41.AutoSize = true;
            this.label41.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.label41.Location = new System.Drawing.Point(586, 190);
            this.label41.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label41.Name = "label41";
            this.label41.Size = new System.Drawing.Size(73, 20);
            this.label41.TabIndex = 7;
            this.label41.Text = "Power : ";
            // 
            // label40
            // 
            this.label40.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.label40.Location = new System.Drawing.Point(533, 230);
            this.label40.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label40.Name = "label40";
            this.label40.Size = new System.Drawing.Size(120, 43);
            this.label40.TabIndex = 6;
            this.label40.Text = "Power Left Right Balance : ";
            // 
            // label39
            // 
            this.label39.AutoSize = true;
            this.label39.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.label39.Location = new System.Drawing.Point(535, 349);
            this.label39.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label39.Name = "label39";
            this.label39.Size = new System.Drawing.Size(130, 20);
            this.label39.TabIndex = 5;
            this.label39.Text = "HR / CC data : ";
            // 
            // label38
            // 
            this.label38.AutoSize = true;
            this.label38.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.label38.Location = new System.Drawing.Point(560, 400);
            this.label38.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label38.Name = "label38";
            this.label38.Size = new System.Drawing.Size(102, 20);
            this.label38.TabIndex = 4;
            this.label38.Text = "US / Euro : ";
            // 
            // label37
            // 
            this.label37.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.label37.Location = new System.Drawing.Point(518, 290);
            this.label37.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label37.Name = "label37";
            this.label37.Size = new System.Drawing.Size(135, 45);
            this.label37.TabIndex = 3;
            this.label37.Text = "Power Pedalling index : ";
            // 
            // label35
            // 
            this.label35.AutoSize = true;
            this.label35.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.label35.Location = new System.Drawing.Point(587, 50);
            this.label35.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label35.Name = "label35";
            this.label35.Size = new System.Drawing.Size(76, 20);
            this.label35.TabIndex = 2;
            this.label35.Text = "Speed : ";
            // 
            // SMode
            // 
            this.SMode.AutoSize = true;
            this.SMode.Font = new System.Drawing.Font("Microsoft Sans Serif", 16F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.SMode.Location = new System.Drawing.Point(521, 10);
            this.SMode.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.SMode.Name = "SMode";
            this.SMode.Size = new System.Drawing.Size(107, 26);
            this.SMode.TabIndex = 1;
            this.SMode.Text = "SMode : ";
            // 
            // dataGridView1
            // 
            this.dataGridView1.AllowUserToAddRows = false;
            this.dataGridView1.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dataGridView1.Location = new System.Drawing.Point(0, 0);
            this.dataGridView1.Name = "dataGridView1";
            this.dataGridView1.RowTemplate.Height = 24;
            this.dataGridView1.Size = new System.Drawing.Size(513, 493);
            this.dataGridView1.TabIndex = 0;
            // 
            // chart_panel
            // 
            this.chart_panel.Controls.Add(this.bitT4);
            this.chart_panel.Controls.Add(this.bitT3);
            this.chart_panel.Controls.Add(this.bitT2);
            this.chart_panel.Controls.Add(this.bitT1);
            this.chart_panel.Controls.Add(this.bit);
            this.chart_panel.Controls.Add(this.label46);
            this.chart_panel.Controls.Add(this.label45);
            this.chart_panel.Controls.Add(this.label23);
            this.chart_panel.Controls.Add(this.power_chart);
            this.chart_panel.Controls.Add(this.hr_chart);
            this.chart_panel.Controls.Add(this.chart);
            this.chart_panel.Controls.Add(this.speed_chart);
            this.chart_panel.Location = new System.Drawing.Point(4, 28);
            this.chart_panel.Name = "chart_panel";
            this.chart_panel.Size = new System.Drawing.Size(780, 509);
            this.chart_panel.TabIndex = 8;
            this.chart_panel.Visible = false;
            // 
            // bitT4
            // 
            this.bitT4.AutoSize = true;
            this.bitT4.Location = new System.Drawing.Point(596, 472);
            this.bitT4.Name = "bitT4";
            this.bitT4.Size = new System.Drawing.Size(39, 12);
            this.bitT4.TabIndex = 11;
            this.bitT4.Text = "label50";
            // 
            // bitT3
            // 
            this.bitT3.AutoSize = true;
            this.bitT3.Location = new System.Drawing.Point(559, 471);
            this.bitT3.Name = "bitT3";
            this.bitT3.Size = new System.Drawing.Size(39, 12);
            this.bitT3.TabIndex = 10;
            this.bitT3.Text = "label49";
            // 
            // bitT2
            // 
            this.bitT2.AutoSize = true;
            this.bitT2.Location = new System.Drawing.Point(495, 472);
            this.bitT2.Name = "bitT2";
            this.bitT2.Size = new System.Drawing.Size(39, 12);
            this.bitT2.TabIndex = 9;
            this.bitT2.Text = "label48";
            // 
            // bitT1
            // 
            this.bitT1.AutoSize = true;
            this.bitT1.Location = new System.Drawing.Point(427, 473);
            this.bitT1.Name = "bitT1";
            this.bitT1.Size = new System.Drawing.Size(39, 12);
            this.bitT1.TabIndex = 8;
            this.bitT1.Text = "label47";
            // 
            // bit
            // 
            this.bit.AutoSize = true;
            this.bit.Location = new System.Drawing.Point(381, 473);
            this.bit.Name = "bit";
            this.bit.Size = new System.Drawing.Size(39, 12);
            this.bit.TabIndex = 7;
            this.bit.Text = "label47";
            // 
            // label46
            // 
            this.label46.AutoSize = true;
            this.label46.Font = new System.Drawing.Font("新細明體", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.label46.Location = new System.Drawing.Point(64, 456);
            this.label46.Name = "label46";
            this.label46.Size = new System.Drawing.Size(212, 16);
            this.label46.TabIndex = 6;
            this.label46.Text = "Button white = Hide the line";
            // 
            // label45
            // 
            this.label45.AutoSize = true;
            this.label45.Font = new System.Drawing.Font("新細明體", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.label45.Location = new System.Drawing.Point(64, 432);
            this.label45.Name = "label45";
            this.label45.Size = new System.Drawing.Size(226, 16);
            this.label45.TabIndex = 5;
            this.label45.Text = "Button yellow = Show the line";
            // 
            // label23
            // 
            this.label23.AutoSize = true;
            this.label23.Font = new System.Drawing.Font("新細明體", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.label23.Location = new System.Drawing.Point(20, 432);
            this.label23.Name = "label23";
            this.label23.Size = new System.Drawing.Size(41, 16);
            this.label23.TabIndex = 4;
            this.label23.Text = "P.S. ";
            // 
            // power_chart
            // 
            this.power_chart.Location = new System.Drawing.Point(650, 435);
            this.power_chart.Margin = new System.Windows.Forms.Padding(2);
            this.power_chart.Name = "power_chart";
            this.power_chart.Size = new System.Drawing.Size(80, 30);
            this.power_chart.TabIndex = 3;
            this.power_chart.Text = "Power Line";
            this.power_chart.UseVisualStyleBackColor = true;
            this.power_chart.MouseClick += new System.Windows.Forms.MouseEventHandler(this.power_chart_MouseClick);
            // 
            // hr_chart
            // 
            this.hr_chart.Location = new System.Drawing.Point(510, 435);
            this.hr_chart.Margin = new System.Windows.Forms.Padding(2);
            this.hr_chart.Name = "hr_chart";
            this.hr_chart.Size = new System.Drawing.Size(80, 30);
            this.hr_chart.TabIndex = 2;
            this.hr_chart.Text = "HR Line";
            this.hr_chart.UseVisualStyleBackColor = true;
            this.hr_chart.MouseClick += new System.Windows.Forms.MouseEventHandler(this.hr_chart_MouseClick);
            // 
            // chart
            // 
            this.chart.Location = new System.Drawing.Point(20, 20);
            this.chart.Margin = new System.Windows.Forms.Padding(4);
            this.chart.Name = "chart";
            this.chart.ScrollGrace = 0D;
            this.chart.ScrollMaxX = 0D;
            this.chart.ScrollMaxY = 0D;
            this.chart.ScrollMaxY2 = 0D;
            this.chart.ScrollMinX = 0D;
            this.chart.ScrollMinY = 0D;
            this.chart.ScrollMinY2 = 0D;
            this.chart.Size = new System.Drawing.Size(748, 370);
            this.chart.TabIndex = 1;
            // 
            // speed_chart
            // 
            this.speed_chart.BackColor = System.Drawing.Color.White;
            this.speed_chart.Location = new System.Drawing.Point(360, 435);
            this.speed_chart.Name = "speed_chart";
            this.speed_chart.Size = new System.Drawing.Size(80, 30);
            this.speed_chart.TabIndex = 0;
            this.speed_chart.Text = "Speed Line";
            this.speed_chart.UseVisualStyleBackColor = false;
            this.speed_chart.MouseClick += new System.Windows.Forms.MouseEventHandler(this.speed_chart_MouseClick);
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 12F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(784, 562);
            this.Controls.Add(this.menuStrip1);
            this.Controls.Add(this.statusStrip1);
            this.Controls.Add(this.HRData_Panel);
            this.Controls.Add(this.Parameter_panel);
            this.Controls.Add(this.chart_panel);
            this.Name = "Form1";
            this.Text = "Form1";
            this.Load += new System.EventHandler(this.Form1_Load);
            this.menuStrip1.ResumeLayout(false);
            this.menuStrip1.PerformLayout();
            this.statusStrip1.ResumeLayout(false);
            this.statusStrip1.PerformLayout();
            this.Parameter_panel.ResumeLayout(false);
            this.Parameter_panel.PerformLayout();
            this.panel1.ResumeLayout(false);
            this.panel1.PerformLayout();
            this.HRData_Panel.ResumeLayout(false);
            this.HRData_Panel.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView1)).EndInit();
            this.chart_panel.ResumeLayout(false);
            this.chart_panel.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion
        private System.Windows.Forms.MenuStrip menuStrip1;
        private System.Windows.Forms.ToolStripMenuItem toolStripMenuItem1;
        private System.Windows.Forms.ToolStripMenuItem openFileToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem viewToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem generalParameterParamsCtrl1ToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem heartRateDataHRDataCtrl2ToolStripMenuItem;
        private System.Windows.Forms.Label versionS;
        private System.Windows.Forms.TextBox versionD;
        private System.Windows.Forms.StatusStrip statusStrip1;
        private System.Windows.Forms.ToolStripStatusLabel filename;
        private System.Windows.Forms.Panel Parameter_panel;
        private System.Windows.Forms.Label lengthS;
        private System.Windows.Forms.Label startTimeS;
        private System.Windows.Forms.Label dateS;
        private System.Windows.Forms.Label monitorS;
        private System.Windows.Forms.TextBox lengthD;
        private System.Windows.Forms.TextBox dateD;
        private System.Windows.Forms.TextBox startTimeD;
        private System.Windows.Forms.TextBox monitorD;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label24;
        private System.Windows.Forms.Label label18;
        private System.Windows.Forms.TextBox restHRD;
        private System.Windows.Forms.TextBox startDelayD;
        private System.Windows.Forms.TextBox vo2maxD;
        private System.Windows.Forms.TextBox weightD;
        private System.Windows.Forms.TextBox time3D;
        private System.Windows.Forms.TextBox time2D;
        private System.Windows.Forms.TextBox activeLimitD;
        private System.Windows.Forms.TextBox maxHRD;
        private System.Windows.Forms.TextBox lower2D;
        private System.Windows.Forms.TextBox upper3D;
        private System.Windows.Forms.TextBox lower3D;
        private System.Windows.Forms.TextBox time1D;
        private System.Windows.Forms.TextBox upper2D;
        private System.Windows.Forms.TextBox lower1D;
        private System.Windows.Forms.TextBox upper1D;
        private System.Windows.Forms.Label label16;
        private System.Windows.Forms.Label label15;
        private System.Windows.Forms.Label label14;
        private System.Windows.Forms.Label label13;
        private System.Windows.Forms.Label label12;
        private System.Windows.Forms.Label label11;
        private System.Windows.Forms.TextBox intervalD;
        private System.Windows.Forms.Label intervalS;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.Label label36;
        private System.Windows.Forms.Label label34;
        private System.Windows.Forms.Label label32;
        private System.Windows.Forms.Label label31;
        private System.Windows.Forms.Label label30;
        private System.Windows.Forms.Label label29;
        private System.Windows.Forms.Label label28;
        private System.Windows.Forms.Label label27;
        private System.Windows.Forms.Label label26;
        private System.Windows.Forms.Label label25;
        private System.Windows.Forms.Panel HRData_Panel;
        private System.Windows.Forms.DataGridView dataGridView1;
        private System.Windows.Forms.TextBox altitudeMaxD;
        private System.Windows.Forms.TextBox HRminD;
        private System.Windows.Forms.TextBox powerAvgD;
        private System.Windows.Forms.TextBox altitudeAvgD;
        private System.Windows.Forms.TextBox HRmaxD;
        private System.Windows.Forms.TextBox powerMaxD;
        private System.Windows.Forms.TextBox speedMaxD;
        private System.Windows.Forms.TextBox speedAvgD;
        private System.Windows.Forms.TextBox HRavgD;
        private System.Windows.Forms.TextBox distanceD;
        private System.Windows.Forms.ToolStripMenuItem generateToolStripMenuItem;
        private System.Windows.Forms.Label label43;
        private System.Windows.Forms.Label label42;
        private System.Windows.Forms.Label label41;
        private System.Windows.Forms.Label label40;
        private System.Windows.Forms.Label label39;
        private System.Windows.Forms.Label label38;
        private System.Windows.Forms.Label label35;
        private System.Windows.Forms.Label SMode;
        private System.Windows.Forms.Button speedON;
        private System.Windows.Forms.Button speedOFF;
        private System.Windows.Forms.Button usOFF;
        private System.Windows.Forms.Button cadenceOFF;
        private System.Windows.Forms.Button altitudeOFF;
        private System.Windows.Forms.Button powerOFF;
        private System.Windows.Forms.Button power_balanceOFF;
        private System.Windows.Forms.Button usON;
        private System.Windows.Forms.Button power_balanceON;
        private System.Windows.Forms.Button powerON;
        private System.Windows.Forms.Button altitudeON;
        private System.Windows.Forms.Button cadenceON;
        private System.Windows.Forms.Label label37;
        private System.Windows.Forms.Button power_indexON;
        private System.Windows.Forms.Button power_indexOFF;
        private System.Windows.Forms.Label label44;
        private System.Windows.Forms.Label label33;
        private System.Windows.Forms.Panel chart_panel;
        private System.Windows.Forms.Button speed_chart;
        private ZedGraph.ZedGraphControl chart;
        private System.Windows.Forms.Button hr_chart;
        private System.Windows.Forms.Button power_chart;
        private System.Windows.Forms.Label distanceT;
        private System.Windows.Forms.Label altMaxT;
        private System.Windows.Forms.Label speedAvgT;
        private System.Windows.Forms.Label speedMaxT;
        private System.Windows.Forms.Label altAvgT;
        private System.Windows.Forms.Label label17;
        private System.Windows.Forms.Label label20;
        private System.Windows.Forms.Label label19;
        private System.Windows.Forms.Label label21;
        private System.Windows.Forms.Label label22;
        private System.Windows.Forms.TextBox ccT;
        private System.Windows.Forms.Label label46;
        private System.Windows.Forms.Label label45;
        private System.Windows.Forms.Label label23;
        private System.Windows.Forms.Label bit;
        private System.Windows.Forms.Label bitT4;
        private System.Windows.Forms.Label bitT3;
        private System.Windows.Forms.Label bitT2;
        private System.Windows.Forms.Label bitT1;
    }
}

