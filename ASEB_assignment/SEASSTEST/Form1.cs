﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.IO;
using System.Text.RegularExpressions;
using System.Collections;
using ZedGraph;

namespace SEASSTEST
{
    public partial class Form1 : Form
    {
        int counter = 0;
        int m_type, column_count, power_tmp, hr_tmp, data_format, data_column, smode_bit;
        int power_max = 0;
        int bit1, bit2, bit3, bit4, bit5;
        int chart_index1, chart_index2, chart_index3;
        double hrdata_sum, powerdata_sum, altitude_sum, speed_sum, time_tmp, speed_tmp;
        string line, file, FILENAME, smode_data, smode_tmp, smode_power_balance, smode_power_index;
        string co2max_data;
        FileStream fs;
        StreamReader sr;
        ArrayList HRDataList = new ArrayList();
        private CurrencyManager currencyManager = null;
        GraphPane graphPaane;
        PointPairList list1, list2, list3;

        public Form1()
        {
            InitializeComponent();
        }

        private void Form1_Load(object sender, EventArgs e)
        {
        }

        public void reset_data ()
        {
            HRDataList = new ArrayList();
            dataGridView1.Rows.Clear();
            hrdata_sum = 0;
            powerdata_sum = 0;
            altitude_sum = 0;
            speed_sum = 0;
            time_tmp = 0;
            speed_tmp = 0;
        } // this is reset the data before open other hrm file

        private void openFileToolStripMenuItem_Click(object sender, EventArgs e)
        {
            reset_data();
            OpenFileDialog openFileDialog1 = new OpenFileDialog();
            openFileDialog1.Filter = "所有檔案(*.*)|*.*|Heart Rate Monitor(*.hrm)|*.hrm";

            if (openFileDialog1.ShowDialog() == DialogResult.OK)
            {
                FILENAME = openFileDialog1.FileName;
                if (Path.GetExtension(FILENAME) == ".hrm")
                {
                    file = openFileDialog1.FileName;
                    this.Text = "檔名：" + openFileDialog1.FileName;
                    fs = new FileStream(file, FileMode.Open);
                    sr = new StreamReader(fs);
                    filename.Text = "File：" + openFileDialog1.FileName;
                    version();
                    monitor();
                    sMode();
                    date();
                    startTime();
                    length();
                    interval();
                    upper1();
                    lower1();
                    upper2();
                    lower2();
                    upper3();
                    lower3();
                    time1();
                    time2();
                    time3();
                    activeLimit();
                    maxHR();
                    restHR();
                    startDelay();
                    vo2max();
                    weight();
                    format();
                    hrdata();
                    hravg();
                    hrMax();
                    hrMin();
                    Parameter_panel.Visible = true;
                }
                else
                {
                    MessageBox.Show("Please open .hrm file!!");
                }
            }
            sr.Close();
        } // open the hrm file and check the is hrm format

        private void generalParameterParamsCtrl1ToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Parameter_panel.Visible = true;
            HRData_Panel.Visible = false;
            chart_panel.Visible = false;
        } // this is show general parameter params panel

        private void heartRateDataHRDataCtrl2ToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Parameter_panel.Visible = false;
            HRData_Panel.Visible = true;
            chart_panel.Visible = false;
        } // this is show heart rate data panel

        private void generateToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Parameter_panel.Visible = false;
            HRData_Panel.Visible = false;
            chart_panel.Visible = true;
        } // this is generate chart panel

        public void version()
        {
            Regex rgx = new Regex(@"Version=\d*", RegexOptions.IgnoreCase);
            while ((line = sr.ReadLine()) != null)
            {
                Match m = rgx.Match(line);
                if (m.Success)
                {
                    String data = line;
                    String version_tmp = data.Remove(0, 8);
                    versionD.Text = version_tmp.Substring(0, 1) + "." + version_tmp.Substring(version_tmp.Length - 2);
                    break;
                }
                counter++;
            }

        } // show the version data

        public void monitor()
        {
            Regex rgx = new Regex(@"Monitor=\d*", RegexOptions.IgnoreCase);
            while ((line = sr.ReadLine()) != null)
            {
                Match m = rgx.Match(line);
                if (m.Success)
                {
                    String data = line;
                    m_type = Int32.Parse(data.Remove(0, 8));
                    break;
                }
                counter++;
            }
            monitor_type monitor_num = new monitor_type();
            String monitor = monitor_num.getMonitorType(m_type);
            monitorD.Text = monitor;
        } // show the monitor data

        public void sMode()
        {
            Regex rgx = new Regex(@"SMode=\d*", RegexOptions.IgnoreCase);
            while ((line = sr.ReadLine()) != null)
            {
                Match m = rgx.Match(line);
                if (m.Success)
                {
                    smode_data = line.Remove(0, 6);
                    break;
                }
                counter++;
            }

            bit1 = Int32.Parse(smode_data.Substring(0, 1));
            bit2 = Int32.Parse(smode_data.Substring(1, 1));
            bit3 = Int32.Parse(smode_data.Substring(2, 1));
            bit4 = Int32.Parse(smode_data.Substring(3, 1));
            bit5 = Int32.Parse(smode_data.Substring(4, 1));
        } // get the smode data and get the each bit

        public void date()
        {
            Regex rgx = new Regex(@"Date=\d*", RegexOptions.IgnoreCase);
            while ((line = sr.ReadLine()) != null)
            {
                Match m = rgx.Match(line);
                if (m.Success)
                {
                    String data = line.Remove(0, 5);
                    String year = data.Substring(0, 4);
                    String mouth = data.Substring(4, 2);
                    String day = data.Substring(data.Length - 2);

                    dateD.Text = year + "/" + mouth + "/" + day;
                    break;
                }
                counter++;
            }
        } // show the date data

        public void startTime()
        {
            Regex rgx = new Regex(@"StartTime=\d*:\d*:\d*\.\d*", RegexOptions.IgnoreCase);
            while ((line = sr.ReadLine()) != null)
            {
                Match m = rgx.Match(line);
                if (m.Success)
                {
                    String data = line;
                    startTimeD.Text = data.Remove(0, 10);
                    break;
                }
                counter++;
            }
        } // show the start time data

        public void length()
        {
            Regex rgx = new Regex(@"Length=\d*:\d*:\d*\.\d*", RegexOptions.IgnoreCase);
            while ((line = sr.ReadLine()) != null)
            {
                Match m = rgx.Match(line);
                if (m.Success)
                {
                    String data = line;
                    lengthD.Text = data.Remove(0, 7);
                    break;
                }
                counter++;
            }
        } // show the length data

        public void interval()
        {
            Regex rgx = new Regex(@"Interval=\d*", RegexOptions.IgnoreCase);
            while ((line = sr.ReadLine()) != null)
            {
                Match m = rgx.Match(line);
                if (m.Success)
                {
                    String data = line;
                    intervalD.Text = data.Remove(0, 9);
                    break;
                }
                counter++;
            }
        } // show interval data

        public void upper1()
        {
            Regex rgx = new Regex(@"Upper1=\d*", RegexOptions.IgnoreCase);
            while ((line = sr.ReadLine()) != null)
            {
                Match m = rgx.Match(line);
                if (m.Success)
                {
                    String data = line;
                    upper1D.Text = data.Remove(0, 7);
                    break;
                }
                counter++;
            }
        } // show the upper1 data

        public void lower1()
        {
            Regex rgx = new Regex(@"Lower1=\d*", RegexOptions.IgnoreCase);
            while ((line = sr.ReadLine()) != null)
            {
                Match m = rgx.Match(line);
                if (m.Success)
                {
                    String data = line;
                    lower1D.Text = data.Remove(0, 7);
                    break;
                }
                counter++;
            }
        } // show the lower1 data

        public void upper2()
        {
            Regex rgx = new Regex(@"Upper2=\d*", RegexOptions.IgnoreCase);
            while ((line = sr.ReadLine()) != null)
            {
                Match m = rgx.Match(line);
                if (m.Success)
                {
                    String data = line;
                    upper2D.Text = data.Remove(0, 7);
                    break;
                }
                counter++;
            }
        } // show upper2 data

        public void lower2()
        {
            Regex rgx = new Regex(@"Lower2=\d*", RegexOptions.IgnoreCase);
            while ((line = sr.ReadLine()) != null)
            {
                Match m = rgx.Match(line);
                if (m.Success)
                {
                    String data = line;
                    lower2D.Text = data.Remove(0, 7);
                    break;
                }
                counter++;
            }
        } // show lower2 data

        public void upper3()
        {
            Regex rgx = new Regex(@"Upper3=\d*", RegexOptions.IgnoreCase);
            while ((line = sr.ReadLine()) != null)
            {
                Match m = rgx.Match(line);
                if (m.Success)
                {
                    String data = line;
                    upper3D.Text = data.Remove(0, 7);
                    break;
                }
                counter++;
            }
        } // show upper3 data

        public void lower3()
        {
            Regex rgx = new Regex(@"Lower3=\d*", RegexOptions.IgnoreCase);
            while ((line = sr.ReadLine()) != null)
            {
                Match m = rgx.Match(line);
                if (m.Success)
                {
                    String data = line;
                    lower3D.Text = data.Remove(0, 7);
                    break;
                }
                counter++;
            }
        } // show lower3 data

        public void time1()
        {
            Regex rgx = new Regex(@"Timer1=\d*:\d*:\d*\.\d*", RegexOptions.IgnoreCase);
            while ((line = sr.ReadLine()) != null)
            {
                Match m = rgx.Match(line);
                if (m.Success)
                {
                    String data = line;
                    time1D.Text = data.Remove(0, 7);
                    break;
                }
                counter++;
            }
        } // show the timw1 data

        public void time2()
        {
            Regex rgx = new Regex(@"Timer2=\d*:\d*:\d*\.\d*", RegexOptions.IgnoreCase);
            while ((line = sr.ReadLine()) != null)
            {
                Match m = rgx.Match(line);
                if (m.Success)
                {
                    String data = line;
                    time2D.Text = data.Remove(0, 7);
                    break;
                }
                counter++;
            }
        } // show the time2 data

        public void time3()
        {
            Regex rgx = new Regex(@"Timer3=\d*:\d*:\d*\.\d*", RegexOptions.IgnoreCase);
            while ((line = sr.ReadLine()) != null)
            {
                Match m = rgx.Match(line);
                if (m.Success)
                {
                    String data = line;
                    time3D.Text = data.Remove(0, 7);
                    break;
                }
                counter++;
            }
        } // show the time3 data

        public void activeLimit()
        {
            Regex rgx = new Regex(@"ActiveLimit=\d*", RegexOptions.IgnoreCase);
            while ((line = sr.ReadLine()) != null)
            {
                Match m = rgx.Match(line);
                if (m.Success)
                {
                    String data = line;
                    activeLimitD.Text = data.Remove(0, 12);
                    break;
                }
                counter++;
            }
        } // show active limit data

        public void maxHR()
        {
            Regex rgx = new Regex(@"MaxHR=\d*", RegexOptions.IgnoreCase);
            while ((line = sr.ReadLine()) != null)
            {
                Match m = rgx.Match(line);
                if (m.Success)
                {
                    String data = line;
                    maxHRD.Text = data.Remove(0, 6);
                    break;
                }
                counter++;
            }
        } // show maxHR data

        public void restHR()
        {
            Regex rgx = new Regex(@"RestHR=\d*", RegexOptions.IgnoreCase);
            while ((line = sr.ReadLine()) != null)
            {
                Match m = rgx.Match(line);
                if (m.Success)
                {
                    String data = line;
                    restHRD.Text = data.Remove(0, 7);
                    break;
                }
                counter++;
            }
        } // show restHR data

        public void startDelay()
        {
            Regex rgx = new Regex(@"StartDelay=\d*", RegexOptions.IgnoreCase);
            while ((line = sr.ReadLine()) != null)
            {
                Match m = rgx.Match(line);
                if (m.Success)
                {
                    String data = line;
                    startDelayD.Text = data.Remove(0, 11);
                    break;
                }
                counter++;
            }
        } // show start delay data

        public void vo2max()
        {
            Regex rgx = new Regex(@"VO2max=\d*", RegexOptions.IgnoreCase);
            while ((line = sr.ReadLine()) != null)
            {
                Match m = rgx.Match(line);
                if (m.Success)
                {
                    co2max_data = line.Remove(0, 7);

                    break;
                }
                counter++;
            }
            if (co2max_data == "1")
            {
                vo2maxD.Text = "Limits 1 and 2";
            }
            else
            {
                vo2maxD.Text = "Treshold limits";
            }

        } // show the vo2max data

        public void weight()
        {
            Regex rgx = new Regex(@"Weight=\d*", RegexOptions.IgnoreCase);
            while ((line = sr.ReadLine()) != null)
            {
                Match m = rgx.Match(line);
                if (m.Success)
                {
                    String data = line;
                    weightD.Text = data.Remove(0, 7);
                    break;
                }
                counter++;
            }
            add_column();
        } // weight data

        public void format ()
        {
            data_format = Int32.Parse(smode_data.Substring(8, 1));
            formatUnit();
        } // get smode 8 bit 

        public void formatUnit ()
        {
            if (data_format == 0)
            {
                distanceT.Text = "km";
                altAvgT.Text = "km/h";
                altMaxT.Text = "km/h";
                speedAvgT.Text = "m";
                speedMaxT.Text = "m";
                for (int i = 0; i < smode_bit; i ++)
                {
                    if (dataGridView1.Columns[i].Name == "speedC")
                    {
                        dataGridView1.Columns[i].HeaderCell.Value = "Speed (km/h)";
                    }
                    if (dataGridView1.Columns[i].Name == "altitudeC")
                    {
                        dataGridView1.Columns[i].HeaderCell.Value = "Altitude (m)";
                    }
                } // change the HR data column name
            } // if smode 8 bit is 0, show Euro format
            else
            {
                distanceT.Text = "miles";
                altAvgT.Text = "mph";
                altMaxT.Text = "mph";
                speedAvgT.Text = "ft";
                speedMaxT.Text = "ft";
                for (int i = 0; i < smode_bit; i++)
                {
                    if (dataGridView1.Columns[i].Name == "speedC")
                    {
                        dataGridView1.Columns[i].HeaderCell.Value = "Speed (mph)";
                    }
                    if (dataGridView1.Columns[i].Name == "altitudeC")
                    {
                        dataGridView1.Columns[i].HeaderCell.Value = "Altitude (ft)";
                    }
                } // change the HR data column name
            } // if smode 8 bit is 1, show US format
        } //change data format

        public void distance()
        {
                double distance_tmp;
                TimeSpan time_lenght = TimeSpan.Parse(lengthD.Text);
                String temp = time_lenght.TotalHours.ToString();
                time_tmp = double.Parse(temp);
                speed_tmp = double.Parse(speedAvgD.Text);
                distance_tmp = time_tmp * speed_tmp;

                distanceD.Text = distance_tmp.ToString("#0.0");
        } // show the distance data

        public void altitudeAvg()
        {
                double altitude_tmp;
                for (int i = 0; i < column_count; i++)
                {
                    altitude_tmp = Int32.Parse(dataGridView1.Rows[i].Cells["altitudeC"].Value.ToString());
                    altitude_sum = altitude_sum + altitude_tmp;
                }
                double altitudeavg = altitude_sum / column_count;
                altitudeAvgD.Text = altitudeavg.ToString("#0.00");
        } // show altitude avg

        public void altitudeMax()
        {
                int altitude_max = 0;
                int altitude_tmp;
                for (int i = 0; i < column_count; i++)
                {
                    altitude_tmp = Int32.Parse(dataGridView1.Rows[i].Cells["altitudeC"].Value.ToString());
                    if (altitude_tmp > altitude_max)
                    {
                        altitude_max = altitude_tmp;
                    }
                }
                altitudeMaxD.Text = altitude_max.ToString();
        } // show Max altitude

        public void speedAvg()
        {
            double speeddata_tmp;
            for (int i = 0; i < column_count; i++)
            {
                speeddata_tmp = Int32.Parse(dataGridView1.Rows[i].Cells["speedC"].Value.ToString());
                speed_sum = speed_sum + speeddata_tmp;
            }
            double speedavg = speed_sum / column_count;
            speedAvgD.Text = speedavg.ToString("#0.00");
        } // show Avg speed

        public void speedMax()
        {
            int speed_max = 0;
            int speed_tmp;
            for (int i = 0; i < column_count; i++)
            {
                speed_tmp = Int32.Parse(dataGridView1.Rows[i].Cells["speedC"].Value.ToString());
                if (speed_tmp > speed_max)
                {
                    speed_max = speed_tmp;
                }
            }
            speedMaxD.Text = speed_max.ToString();
        } // show Max speed

        public void powerAvg()
        {

            double powerdata_tmp;
            for (int i = 0; i < column_count; i++)
            {
                powerdata_tmp = Int32.Parse(dataGridView1.Rows[i].Cells["powerC"].Value.ToString());
                powerdata_sum = powerdata_sum + powerdata_tmp;
            }
            double poweravg = powerdata_sum / column_count;
            powerAvgD.Text = poweravg.ToString("#0.00");
        } // show avg power

        public void powerMax()
        {
            for (int i = 0; i < column_count; i++)
            {
                power_tmp = Int32.Parse(dataGridView1.Rows[i].Cells["powerC"].Value.ToString());
                if (power_tmp > power_max)
                {
                    power_max = power_tmp;
                }
            }
            powerMaxD.Text = power_max.ToString();
        } // show max power

        public void hravg()
        {
            int hravg_count = dataGridView1.Columns.Count;
            double hrdata_tmp;
            for (int i = 0; i < hravg_count; i++)
            {
                hrdata_tmp = Int32.Parse(dataGridView1.Rows[i].Cells["HRC"].Value.ToString());
                hrdata_sum = hrdata_sum + hrdata_tmp;
            }
            double hravg = hrdata_sum / hravg_count;
            HRavgD.Text = hravg.ToString("#0.00");
        } // show avg HR data

        public void hrMax()
        {
            int hr_max = 0;
            for (int i = 0; i < column_count; i++)
            {
                hr_tmp = Int32.Parse(dataGridView1.Rows[i].Cells["HRC"].Value.ToString());
                if (hr_tmp > hr_max)
                {
                    hr_max = hr_tmp;
                }
            }
            HRmaxD.Text = hr_max.ToString();
        } // show Max HR data

        public void hrMin()
        {
            int hr_min = 1000;
            for (int i = 0; i < column_count; i++)
            {
                hr_tmp = Int32.Parse(dataGridView1.Rows[i].Cells["HRC"].Value.ToString());
                if (hr_tmp < hr_min)
                {
                    hr_min = hr_tmp;
                }
            }
            HRminD.Text = hr_min.ToString();
        } // show Min HR data

        public void hrdata()
        {
            Regex rgx = new Regex(@"\[HRData\]", RegexOptions.IgnoreCase);
            while ((line = sr.ReadLine()) != null)
            {
                Match m = rgx.Match(line);
                if (m.Success)
                {
                    counter++;
                    break;
                }
                counter++;
            }
            data();
        } // get the HR data header

        public void data()
        {
            while ((line = sr.ReadLine()) != null)
            {
                HRDataList.Add(line);
                counter++;
            }
            showdata();
        } // get HR data to array list

        public void add_column ()
        {
            string tmp = smode_data.Substring(0, 5);

            // how many smode bit is 1, and then +2
            smode_bit = Regex.Matches(tmp, "1").Count + 2;
            
            dataGridView1.ColumnCount = smode_bit;

            // create time and HR data column
            dataGridView1.Columns[0].Name = "timeC";
            dataGridView1.Columns[0].HeaderCell.Value = "Time";
            dataGridView1.Columns[1].Name = "HRC";
            dataGridView1.Columns[1].HeaderCell.Value = "Heart Rate (bpm)";

            int bit = Int32.Parse(tmp);
            switch (bit)
            {
                case 11111:
                    dataGridView1.Columns[2].Name = "speedC";
                    dataGridView1.Columns[3].Name = "cadenceC";
                    dataGridView1.Columns[3].HeaderCell.Value = "Cadence (rpm)";
                    dataGridView1.Columns[4].Name = "altitudeC";
                    dataGridView1.Columns[5].Name = "powerC";
                    dataGridView1.Columns[5].HeaderCell.Value = "Power (Watts)";
                    dataGridView1.Columns[6].Name = "power_balanceC";
                    dataGridView1.Columns[6].HeaderCell.Value = "Power Balance and Pedalling Index";
                    break;
                case 00000:
                    break;
                case 01111:
                    dataGridView1.Columns[2].Name = "cadenceC";
                    dataGridView1.Columns[2].HeaderCell.Value = "Cadence (rpm)";
                    dataGridView1.Columns[3].Name = "altitudeC";
                    dataGridView1.Columns[3].Name = "powerC";
                    dataGridView1.Columns[4].HeaderCell.Value = "Power (Watts)";
                    dataGridView1.Columns[4].Name = "power_balanceC";
                    dataGridView1.Columns[5].HeaderCell.Value = "Power Balance and Pedalling Index";
                    break;
                case 10111:
                    dataGridView1.Columns[2].Name = "speedC";
                    dataGridView1.Columns[3].Name = "altitudeC";
                    dataGridView1.Columns[3].Name = "powerC";
                    dataGridView1.Columns[4].HeaderCell.Value = "Power (Watts)";
                    dataGridView1.Columns[5].Name = "power_balanceC";
                    dataGridView1.Columns[5].HeaderCell.Value = "Power Balance and Pedalling Index";
                    break;
                case 11011:
                    dataGridView1.Columns[2].Name = "speedC";
                    dataGridView1.Columns[3].Name = "cadenceC";
                    dataGridView1.Columns[3].HeaderCell.Value = "Cadence (rpm)";
                    dataGridView1.Columns[4].Name = "powerC";
                    dataGridView1.Columns[4].HeaderCell.Value = "Power (Watts)";
                    dataGridView1.Columns[5].Name = "power_balanceC";
                    dataGridView1.Columns[5].HeaderCell.Value = "Power Balance and Pedalling Index";
                    break;
                case 11101:
                    dataGridView1.Columns[2].Name = "speedC";
                    dataGridView1.Columns[3].Name = "cadenceC";
                    dataGridView1.Columns[3].HeaderCell.Value = "Cadence (rpm)";
                    dataGridView1.Columns[4].Name = "altitudeC";
                    dataGridView1.Columns[5].Name = "power_balanceC";
                    dataGridView1.Columns[5].HeaderCell.Value = "Power Balance and Pedalling Index";
                    break;
                case 11110:
                    dataGridView1.Columns[2].Name = "speedC";
                    dataGridView1.Columns[3].Name = "cadenceC";
                    dataGridView1.Columns[3].HeaderCell.Value = "Cadence (rpm)";
                    dataGridView1.Columns[4].Name = "altitudeC";
                    dataGridView1.Columns[5].Name = "powerC";
                    dataGridView1.Columns[5].HeaderCell.Value = "Power (Watts)";
                    break;
                case 10000:
                    dataGridView1.Columns[2].Name = "speedC";
                    break;
                case 01000:
                    dataGridView1.Columns[2].Name = "cadenceC";
                    dataGridView1.Columns[2].HeaderCell.Value = "Cadence (rpm)";
                    break;
                case 00100:
                    dataGridView1.Columns[2].Name = "altitudeC";
                    break;
                case 00010:
                    dataGridView1.Columns[2].Name = "powerC";
                    dataGridView1.Columns[2].HeaderCell.Value = "Power (Watts)";
                    break;
                case 00001:
                    dataGridView1.Columns[2].Name = "power_balanceC";
                    dataGridView1.Columns[2].HeaderCell.Value = "Power Balance and Pedalling Index";
                    break;
                case 00111:
                    dataGridView1.Columns[2].Name = "altitudeC";
                    dataGridView1.Columns[3].Name = "powerC";
                    dataGridView1.Columns[3].HeaderCell.Value = "Power (Watts)";
                    dataGridView1.Columns[4].Name = "power_balanceC";
                    dataGridView1.Columns[4].HeaderCell.Value = "Power Balance and Pedalling Index";
                    break;
                case 01011:
                    dataGridView1.Columns[2].Name = "cadenceC";
                    dataGridView1.Columns[2].HeaderCell.Value = "Cadence (rpm)";
                    dataGridView1.Columns[3].Name = "powerC";
                    dataGridView1.Columns[3].HeaderCell.Value = "Power (Watts)";
                    dataGridView1.Columns[4].Name = "power_balanceC";
                    dataGridView1.Columns[4].HeaderCell.Value = "Power Balance and Pedalling Index";
                    break;
                case 01101:
                    dataGridView1.Columns[2].Name = "cadenceC";
                    dataGridView1.Columns[2].HeaderCell.Value = "Cadence (rpm)";
                    dataGridView1.Columns[3].Name = "altitudeC";
                    dataGridView1.Columns[4].Name = "power_balanceC";
                    dataGridView1.Columns[4].HeaderCell.Value = "Power Balance and Pedalling Index";
                    break;
                case 01110:
                    dataGridView1.Columns[2].Name = "cadenceC";
                    dataGridView1.Columns[2].HeaderCell.Value = "Cadence (rpm)";
                    dataGridView1.Columns[3].Name = "altitudeC";
                    dataGridView1.Columns[4].Name = "powerC";
                    dataGridView1.Columns[4].HeaderCell.Value = "Power (Watts)";
                    break;
                case 10011:
                    dataGridView1.Columns[2].Name = "speedC";
                    dataGridView1.Columns[3].Name = "powerC";
                    dataGridView1.Columns[3].HeaderCell.Value = "Power (Watts)";
                    dataGridView1.Columns[4].Name = "power_balanceC";
                    dataGridView1.Columns[4].HeaderCell.Value = "Power Balance and Pedalling Index";
                    break;
                case 10101:
                    dataGridView1.Columns[2].Name = "speedC";
                    dataGridView1.Columns[3].Name = "altitudeC";
                    dataGridView1.Columns[4].Name = "power_balanceC";
                    dataGridView1.Columns[4].HeaderCell.Value = "Power Balance and Pedalling Index";
                    break;
                case 10110:
                    dataGridView1.Columns[2].Name = "speedC";
                    dataGridView1.Columns[3].Name = "altitudeC";
                    dataGridView1.Columns[4].Name = "powerC";
                    dataGridView1.Columns[4].HeaderCell.Value = "Power (Watts)";
                    break;
                case 11001:
                    dataGridView1.Columns[2].Name = "speedC";
                    dataGridView1.Columns[3].Name = "cadenceC";
                    dataGridView1.Columns[3].HeaderCell.Value = "Cadence (rpm)";
                    dataGridView1.Columns[4].Name = "power_balanceC";
                    dataGridView1.Columns[4].HeaderCell.Value = "Power Balance and Pedalling Index";
                    break;
                case 11010:
                    dataGridView1.Columns[2].Name = "speedC";
                    dataGridView1.Columns[3].Name = "cadenceC";
                    dataGridView1.Columns[3].HeaderCell.Value = "Cadence (rpm)";
                    dataGridView1.Columns[4].Name = "powerC";
                    dataGridView1.Columns[4].HeaderCell.Value = "Power (Watts)";
                    break;
                case 11100:
                    dataGridView1.Columns[2].Name = "speedC";
                    dataGridView1.Columns[3].Name = "cadenceC";
                    dataGridView1.Columns[3].HeaderCell.Value = "Cadence (rpm)";
                    dataGridView1.Columns[4].Name = "altitudeC";
                    break;
                case 11000:
                    dataGridView1.Columns[2].Name = "speedC";
                    dataGridView1.Columns[3].Name = "cadenceC";
                    dataGridView1.Columns[3].HeaderCell.Value = "Cadence (rpm)";
                    break;
                case 10100:
                    dataGridView1.Columns[2].Name = "speedC";
                    dataGridView1.Columns[3].Name = "altitudeC";
                    break;
                case 10010:
                    dataGridView1.Columns[2].Name = "speedC";
                    dataGridView1.Columns[3].Name = "powerC";
                    dataGridView1.Columns[3].HeaderCell.Value = "Power (Watts)";
                    break;
                case 10001:
                    dataGridView1.Columns[2].Name = "speedC";
                    dataGridView1.Columns[3].Name = "power_balanceC";
                    dataGridView1.Columns[3].HeaderCell.Value = "Power Balance and Pedalling Index";
                    break;
                case 01100:
                    dataGridView1.Columns[2].Name = "cadenceC";
                    dataGridView1.Columns[2].HeaderCell.Value = "Cadence (rpm)";
                    dataGridView1.Columns[3].Name = "altitudeC";
                    break;
                case 01010:
                    dataGridView1.Columns[2].Name = "cadenceC";
                    dataGridView1.Columns[2].HeaderCell.Value = "Cadence (rpm)";
                    dataGridView1.Columns[3].Name = "powerC";
                    dataGridView1.Columns[3].HeaderCell.Value = "Power (Watts)";
                    break;
                case 01001:
                    dataGridView1.Columns[2].Name = "cadenceC";
                    dataGridView1.Columns[2].HeaderCell.Value = "Cadence (rpm)";
                    dataGridView1.Columns[3].Name = "power_balanceC";
                    dataGridView1.Columns[3].HeaderCell.Value = "Power Balance and Pedalling Index";
                    break;
                case 00110:
                    dataGridView1.Columns[2].Name = "altitudeC";
                    dataGridView1.Columns[3].Name = "powerC";
                    dataGridView1.Columns[3].HeaderCell.Value = "Power (Watts)";
                    break;
                case 00101:
                    dataGridView1.Columns[2].Name = "altitudeC";
                    dataGridView1.Columns[3].Name = "power_balanceC";
                    dataGridView1.Columns[3].HeaderCell.Value = "Power Balance and Pedalling Index";
                    break;
                case 00011:
                    dataGridView1.Columns[2].Name = "powerC";
                    dataGridView1.Columns[2].HeaderCell.Value = "Power (Watts)";
                    dataGridView1.Columns[3].Name = "power_balanceC";
                    dataGridView1.Columns[3].HeaderCell.Value = "Power Balance and Pedalling Index";
                    break;
            } // this is all smode pattern 
        } // get the smode pattern and add the HR data column

        public void showdata()
        {
            int[] hrdata = new int[smode_bit];
            TimeSpan time_start = TimeSpan.Parse(startTimeD.Text);
            TimeSpan second = TimeSpan.FromSeconds(1);
            for (int i = 0; i < HRDataList.Count; i++)
            {
                string value = HRDataList[i].ToString();
                Match match = Regex.Match(value, @"\d*\w");
                for (int j = 1; j < smode_bit; j++)
                {

                    hrdata[j] = Int32.Parse(match.Value);
                    match = match.NextMatch();
                } // add all HR data to datagridview

                switch (smode_bit)
                {
                    case 2:
                        this.dataGridView1.Rows.Add(time_start, hrdata[1]);
                        break;
                    case 3:
                        this.dataGridView1.Rows.Add(time_start, hrdata[1], hrdata[2]);
                        break;
                    case 4:
                        this.dataGridView1.Rows.Add(time_start, hrdata[1], hrdata[2], hrdata[3]);
                        break;
                    case 5:
                        this.dataGridView1.Rows.Add(time_start, hrdata[1], hrdata[2], hrdata[3], hrdata[4]);
                        break;
                    case 6:
                        this.dataGridView1.Rows.Add(time_start, hrdata[1], hrdata[2], hrdata[3], hrdata[4], hrdata[5]);
                        break;
                    case 7:
                        this.dataGridView1.Rows.Add(time_start, hrdata[1], hrdata[2], hrdata[3], hrdata[4], hrdata[5], hrdata[6]);
                        break;
                } // show how many column
                TimeSpan time_tmp = time_start.Add(second);
                time_start = time_tmp;
            }
            smode_check();
            smode_setting();
        }

        public void smode_check ()
        {
            column_count = dataGridView1.Rows.Count;
            if (bit1 == 1)
            {
                speedAvg();
                speedMax();
                distance();
            } else
            {
                distanceD.Text = "Error";
                speedAvgD.Text = "0";
                speedMaxD.Text = "0";
                speedOFF.Enabled = false;
                speedON.Enabled = false;
            }
            if (bit3 == 1)
            {
                altitudeAvg();
                altitudeMax();
            } else
            {
                altitudeAvgD.Text = "0";
                altitudeMaxD.Text = "0";
                altitudeOFF.Enabled = false;
                altitudeON.Enabled = false;
            }
            if (bit4 == 1)
            {
                powerAvg();
                powerMax();
            } else
            {
                powerAvgD.Text = "0";
                powerMaxD.Text = "0";
                powerOFF.Enabled = false;
                powerON.Enabled = false;
            }
        } // if smode data have not speed, altitude or power data, it can show 0

        public void smode_setting()
        {

            if (bit1 == 1)
            {
                speedOFF.Enabled = true;
                speedON.Enabled = false;
            }
            else
            {
                speedOFF.Enabled = false;
                speedON.Enabled = false;
            }

            if (bit2 == 1)
            {
                cadenceOFF.Enabled = true;
                cadenceON.Enabled = false;
            }
            else
            {
                cadenceOFF.Enabled = false;
                cadenceON.Enabled = false;
            }

            if (bit3 == 1)
            {
                altitudeOFF.Enabled = true;
                altitudeON.Enabled = false;
            }
            else
            {
                altitudeOFF.Enabled = false;
                altitudeON.Enabled = false;
            }

            if (bit4 == 1)
            {
                powerOFF.Enabled = true;
                powerON.Enabled = false;
            }
            else
            {
                powerOFF.Enabled = false;
                powerON.Enabled = false;
            }
            
            if (bit5 == 1)
            {
                power_balanceOFF.Enabled = true;
                power_balanceON.Enabled = false;
                power_indexOFF.Enabled = true;
                power_indexON.Enabled = false;
            }
            else
            {
                power_balanceOFF.Enabled = false;
                power_balanceON.Enabled = false;
                power_indexOFF.Enabled = false;
                power_indexON.Enabled = false;
            }
            String smode_cc = smode_data.Substring(6, 1);
            if (smode_cc == "0")
            {
                ccT.Text = "HR data only";
            }
            else
            {
                ccT.Text = "HR + cycling data";
            }
            String smode_us = smode_data.Substring(7, 1);
            if (smode_us == "1")
            {
                usOFF.Enabled = true;
                usON.Enabled = false;
                formatUnit();
            }
            else
            {
                usOFF.Enabled = false;
                usON.Enabled = true;
                formatUnit();
            }
        } // smode button setting

        private void speedon(object sender, MouseEventArgs e)
        {
            speedOFF.Enabled = true;
            speedON.Enabled = false;
            this.dataGridView1.Columns[2].Visible = true;
        }

        private void speedoff(object sender, MouseEventArgs e)
        {
            speedOFF.Enabled = false;
            speedON.Enabled = true;
            this.dataGridView1.Columns[2].Visible = false;
        }

        private void cadenceon(object sender, MouseEventArgs e)
        {
            cadenceOFF.Enabled = true;
            cadenceON.Enabled = false;
            this.dataGridView1.Columns[3].Visible = true;
        }

        private void cadenceoff(object sender, MouseEventArgs e)
        {
            cadenceOFF.Enabled = false;
            cadenceON.Enabled = true;
            this.dataGridView1.Columns[3].Visible = false;
        }

        private void altitudeon(object sender, MouseEventArgs e)
        {
            altitudeOFF.Enabled = true;
            altitudeON.Enabled = false;
            this.dataGridView1.Columns[4].Visible = true;
        }

        private void altitudeoff(object sender, MouseEventArgs e)
        {
            altitudeOFF.Enabled = false;
            altitudeON.Enabled = true;
            this.dataGridView1.Columns[4].Visible = false;
        }

        private void poweron(object sender, MouseEventArgs e)
        {
            powerOFF.Enabled = true;
            powerON.Enabled = false;
            this.dataGridView1.Columns[5].Visible = true;
        }

        private void poweroff(object sender, MouseEventArgs e)
        {
            powerOFF.Enabled = false;
            powerON.Enabled = true;
            this.dataGridView1.Columns[5].Visible = false;
        }

        private void power_balanceon(object sender, MouseEventArgs e)
        {
            power_balanceOFF.Enabled = true;
            power_balanceON.Enabled = false;
            power_indexOFF.Enabled = true;
            power_indexON.Enabled = false;
            this.dataGridView1.Columns[6].Visible = true;
        }

        private void power_balanceoff(object sender, MouseEventArgs e)
        {
            power_balanceOFF.Enabled = false;
            power_balanceON.Enabled = true;
            power_indexOFF.Enabled = false;
            power_indexON.Enabled = true;
            if (power_balanceOFF.Enabled == false && power_indexOFF.Enabled == false)
            {
                this.dataGridView1.Columns[6].Visible = false;
            }
        }

        private void power_indexon(object sender, MouseEventArgs e)
        {
            power_indexOFF.Enabled = true;
            power_indexON.Enabled = false;
            power_balanceOFF.Enabled = true;
            power_balanceON.Enabled = false;
            this.dataGridView1.Columns[6].Visible = true;
        }

        private void power_indexoff(object sender, MouseEventArgs e)
        {
            power_indexOFF.Enabled = false;
            power_indexON.Enabled = true;
            power_balanceOFF.Enabled = false;
            power_balanceON.Enabled = true;
            if (power_balanceOFF.Enabled == false && power_indexOFF.Enabled == false)
            {
                this.dataGridView1.Columns[6].Visible = false;
            }
        }

        private void uson(object sender, MouseEventArgs e)
        {
            usOFF.Enabled = true;
            usON.Enabled = false;
            dataGridView1.Columns[2].HeaderCell.Value = "Speed (mph)";
            dataGridView1.Columns[4].HeaderCell.Value = "Altitude (ft)";
            data_format = 1;
            formatUnit();

        }

        private void usoff(object sender, MouseEventArgs e)
        {
            usOFF.Enabled = false;
            usON.Enabled = true;
            dataGridView1.Columns[2].HeaderCell.Value = "Speed (km/h)";
            dataGridView1.Columns[4].HeaderCell.Value = "Altitude (m)";
            data_format = 0;
            formatUnit();
        }

        public void chart_setting ()
        {
            graphPaane = chart.GraphPane;

            graphPaane.Title.Text = "Chart";
            graphPaane.XAxis.Title.Text = "Data Number of Times (per minute)";
            graphPaane.YAxis.Title.Text = "Data";
            graphPaane.YAxis.Scale.Min = 0;
            graphPaane.XAxis.Scale.Min = 0;
        }

        private void speed_chart_MouseClick(object sender, MouseEventArgs e)
        {
            if (list1 == null && bit1 == 1)
            {
                speed_chart.BackColor = Color.Yellow;
                chart_setting();
                double x, y1;
                list1 = new PointPairList();
                int data_temp = 60;

                for (int i = 0; i < column_count; i++)
                {
                    x = i;
                    y1 = Int32.Parse(dataGridView1.Rows[i].Cells["speedC"].Value.ToString());

                    if (data_temp == i)
                    {
                        list1.Add(x, y1);
                        data_temp = data_temp + 60;
                    }
                }

                LineItem myCurve = graphPaane.AddCurve("Speed",
                      list1, Color.Red, SymbolType.None);
                chart_index1 = chart.GraphPane.CurveList.IndexOf("Speed");

                chart.AxisChange();
            }
            else if (speed_chart.BackColor == Color.White && bit1 == 1)
            {
                speed_chart.BackColor = Color.Yellow;
                chart.GraphPane.CurveList[chart_index1].IsVisible = true;
            }
            else if (speed_chart.BackColor == Color.Yellow && bit1 == 1)
            {
                speed_chart.BackColor = Color.White;
                chart.GraphPane.CurveList[chart_index1].IsVisible = false;
            } else
            {
                MessageBox.Show("Your hrm file has not speed data!!");
            }
            chart.Refresh();
        }

        private void hr_chart_MouseClick(object sender, MouseEventArgs e)
        {
            if (list2 == null )
            {
                hr_chart.BackColor = Color.Yellow;
                chart_setting();
                double x, y2;
                list2 = new PointPairList();
                int data_temp = 60;

                for (int i = 0; i < column_count; i++)
                {
                    x = i;
                  y2 = Int32.Parse(dataGridView1.Rows[i].Cells["HRC"].Value.ToString());
                
                   if (data_temp == i)
                  {
                        list2.Add(x, y2);
                        data_temp = data_temp + 60;
                  }
                }

                LineItem myCurve = graphPaane.AddCurve("Heart Rate",
                     list2, Color.Yellow, SymbolType.None);
                chart_index2 = chart.GraphPane.CurveList.IndexOf("Heart Rate");

                chart.AxisChange();
            }
            else if (hr_chart.BackColor == Color.White)
            {
                hr_chart.BackColor = Color.Yellow;
                chart.GraphPane.CurveList[chart_index2].IsVisible = true;
            }
            else
            {
                hr_chart.BackColor = Color.White;
                chart.GraphPane.CurveList[chart_index2].IsVisible = false;
                
            }
            chart.Refresh();
        }

        private void power_chart_MouseClick(object sender, MouseEventArgs e)
        {
            if (list3 == null && bit4 == 1)
            {
                power_chart.BackColor = Color.Yellow;
                chart_setting();
                // Make up some data arrays based on the Sine function
                double x, y3;
                list3 = new PointPairList();
                int data_temp = 60;

                for (int i = 0; i < column_count; i++)
                {
                    x = i;
                    y3 = Int32.Parse(dataGridView1.Rows[i].Cells["powerC"].Value.ToString());

                    if (data_temp == i)
                    {
                        list3.Add(x, y3);
                        data_temp = data_temp + 60;
                    }
                }
                LineItem myCurve = graphPaane.AddCurve("Power",
                    list3, Color.Green, SymbolType.None);
                chart_index3 = chart.GraphPane.CurveList.IndexOf("Power");

                chart.AxisChange();
            }
            else if (power_chart.BackColor == Color.White && bit4 == 1)
            {
                power_chart.BackColor = Color.Yellow;
                chart.GraphPane.CurveList[chart_index3].IsVisible = true;
            }
            else if (power_chart.BackColor == Color.Yellow && bit4 == 1)
            {
                power_chart.BackColor = Color.White;
                chart.GraphPane.CurveList[chart_index3].IsVisible = false;
            }
            else
            {
                MessageBox.Show("Your hrm file has not power data!!");
            }
            chart.Refresh();
        }
    }
}
